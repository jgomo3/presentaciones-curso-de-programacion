#+TITLE: Fuentes de informacion y como descargarla
#+AUTHOR: Camilo Carrasco Díaz
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+BEAMER_HEADER: \setbeamertemplate{navigation symbols}{}
#+COLUMNS: %5ITEM %10BEAMER_ENV(Env) %5BEAMER_ACT(Act) %6BEAMER_COL(Col)
#+latex_class_options: [10pt]

* Temario de la clase
** ¿Qué veremos en esta clase?
- Descargar información :: Veremos las fuentes para descargar:
 - documentos de texto - archivos de texto
 - música, podcasts - archivos de audio
 - documentales o peliculas - archivos de video
 - subtitulos - archivos de texto plano

- Procesar audio y video :: Veremos como procesar audio o video de manera manual.

- Usar herramientas chown y chmod :: Veremos como administrar las propiedades de los archivos, privilegios y su propietario

- Usar programas P2P :: Veremos como descargar archivos de fuentes confiables

* Registrar inicio de clases
** Una marca en la línea de comandos: con nombre de clase
Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *Descargar*.

#+BEGIN_SRC bash
echo "CLASE_11::Descargar"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_11::Descargar*

* Preparar el navegador
** Instalar un bloqueador de publicidad
En primer lugar, para navegar seguros y  mantenernos alejados de los virus (y la publicidad, que son parte de lo mismo) es recomendable instalar un bloqueador de publicidad para nuestro navegador

Bloqueadores de publicidad:
- Adblock :: Mas antiguo, sin codigo fuente publicado. Permite listas blancas a cambio de dinero (No bloquea toda publicidad)
- Adblock plus :: Continuación del proyecto anterior, tambien terminó permitiendo listas blancas
- Ublock :: De codigo libre, tambien permite listas blancas
- Ublock origin :: Fork del anterior, vuelve a los origenes a bloquear todo sin traicionar al usuario

Instalaremos *ublock origin* de la página de extensiones de firefox:
https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/

* Instalar youtube-dl
** Introduccion a youtube-dl
- Para lograr descargar videos de youtube, podemos usar la herramienta youtube-dl.

- Para ello ingresamos a la página oficial:
 - Página oficial :: https://youtube-dl.org/
 - Repositorio oficial :: https://ytdl-org.github.io/youtube-dl/index.html

Si ingresaste por https://youtube-dl.org, entonces hacer click en el botón "Homepage"
[[file:./img/01.png]]

** Sitios soportados por youtube-dl
- Podemos leer que en la descripción dice, traducido al español:
 /youtube-dl es un programa de línea de comandos para descargar videos de youtube.com y algunos *sitios más*./

- Podemos fijarnos que donde dice *more sites*, hay un hipervinculo que nos lleva a la página con la lista completa de sitios soportados:
 https://ytdl-org.github.io/youtube-dl/supportedsites.html

** Instalando youtube-dl

Ingresamos al boton "Download", que tendrá las instrucciones de instalación
[[file:./img/02.png]]

** Tenemos dos opciones para instalarlo.

- Como especifican en la página de descarga, tenemos dos opciones para instalarlo.
- Instalar en dos pasos. Cada línea es un paso.
- No lo haremos así, para aprender :D

*** Prerequisito, instalar curl y wget
#+BEGIN_SRC BASH
sudo apt install curl wget
#+END_SRC

*** Primera opción, usando curl:
#+BEGIN_SRC bash
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
#+END_SRC

*** Segunda opción, usando wget:
#+BEGIN_SRC bash
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
#+END_SRC

** Realizando el proceso manualmente
*** 1. Nos posicionamos en /usr/local/bin
#+BEGIN_SRC bash
cd /usr/local/bin
#+END_SRC

*** 2. Descargando youtube-dl con wget.
#+BEGIN_SRC bash
sudo wget https://yt-dl.org/downloads/latest/youtube-dl
#+END_SRC

- 2.1 Comprobar derechos 

#+BEGIN_SRC bash
ls -l | grep youtube-dl
-rw-r--r--  1 root   root   1767501 oct 15 17:28 youtube-dl
#+END_SRC

*** 3. Dar derechos de ejecución (x) y lectura (r)
#+BEGIN_SRC bash
sudo chmod a+rx youtube-dl
#+END_SRC

- 3.1 Comprobar derechos
#+BEGIN_SRC bash
ls -l | grep youtube-dl
-rw-r-xr-x  1 root   root   1767501 oct 15 17:28 youtube-dl
#+END_SRC

** Detalle paso 2: Comprobando propietario y grupo

El paso anterior se ejecutó con sudo, debido a que la carpeta
//usr/local/bin/ no pertenece a nuestro usuario

*** Al ejecutar los siguientes comandos, comprobamos que */usr/local/bin* pertenece al usuario *root*:
#+BEGIN_SRC bash
ls -l /usr/local/ | grep bin
drwxr-xr-x  2 root root 4096 oct 14 12:20 bin
#+END_SRC

*** Podemos contrastarlo con nuestra carpeta *~/Documentos* ubicado en la carpeta personal del *usuario*
#+BEGIN_SRC bash
ls -l ~ | grep Documentos
drwxr-xr-x  7 usario usuario 4096 oct 15 14:13 Documentos
#+END_SRC

* Introducción a chown y chmod
** Comprobando nuestro usuario y los grupos al que pertenece
- Para saber nuestro usuario actual:
#+BEGIN_SRC bash
whoami
#+END_SRC

- Para saber a que grupos pertenece nuestro usuario, usamos el comando groups:
#+BEGIN_SRC bash
groups
#+END_SRC

- La sintaxis general es:
#+BEGIN_SRC bash
groups <usuario>
#+END_SRC

- Otras opciones para ver los grupos son:
#+BEGIN_SRC bash
groups usuario
whoami | xargs groups
groups root
#+END_SRC

** Aprendiendo a usar chown
El comando *chown* sirve para cambiar el propietario del archivo y la información del grupo de usuario.
*** La sintaxis general es: 
#+BEGIN_SRC bash
chown [usuario[:grupo]] archivo
#+END_SRC

** Aprendiendo a usar chown
- Por ejemplo, creamos un archivo como super usuario:
#+BEGIN_SRC bash
sudo touch archivo
ls -l | grep archivo
-rw-r--r--  1 root   root         0 oct 16 01:58 archivo
#+END_SRC

- Modificamos su propiedad desde el usuario /root/ al usuario /usario/
#+BEGIN_SRC bash
sudo chown usuario:usuario archivo
#+END_SRC

- Comprobamos la modificacion de propiedad
#+BEGIN_SRC bash
ls -l | grep archivo
-rw-r--r--  1 usuario usuario     0 oct 16 01:58 archivo
#+END_SRC

** Aprendiendo a usar chmod
*chmod* sirve para cambiar los permisos de acceso al archivo.

*** Para los usuarios:
- u :: user - usuario
- g :: group - miembro del grupo
- o :: others - otros
- a :: all - todos los usuarios anteriores (u+g+o)

*** Si deseamos modificar las propiedades de:
- r :: read - lectura
- w :: write - escritura
- x :: execute - ejecución

*** La sintaxis general es: 
#+BEGIN_SRC bash
chmod u=rwx,g=rx,o=r archivo
chmod a+rwx archivo
chmod a-rwx archivo
#+END_SRC

** Aprendiendo a usar chmod

- A modo de ejemplo:
#+BEGIN_SRC bash
ls -l | grep archivo
-rw-r--r--  1 usuario usuario       0 oct 16 01:58 archivo
#+END_SRC

- Modificando los permisos:
#+BEGIN_SRC bash
chmod u=rwx,g=rx,o=r archivo
#+END_SRC

- Comprobando la modificacion de los permisos
#+BEGIN_SRC bash
ls -l | grep archivo
-rwxr-xr--  1 usuario usuario       0 oct 16 01:58 archivo
#+END_SRC

** Aprendiendo a usar chmod
En el ejemplo anterior:
- El /usuario/ propietario del archivo puede /leer, escribir y ejecutar/ el archivo
- Los usuarios pertenecientes al /grupo/ definido en el archivo, pueden /leer y ejecutar/ el archivo
- Los /otros/ usuarios que no sean el usuario propietario del archivo ni que pertenezcan al grupo, pueden solo /leer/ el archivo

* Usar youtube-dl
** Determinar qué buscar
Gastón Soublette
- Filósofo, musicólogo y esteta chileno. 
- Estudió arquitectura y derecho en Chile, musica y musicología en Francia.
- Se desempeñó como director del Instituto de Estética de la Universidad Católica entre 1978 y 1980 y luego como profesor.
- Trabajó con exponentes de la cultura popular chilena, tales como la folcloristas Margot Loyola, Héctor Pavez y Gabriela Pizarro, además de Violeta Parra. 

Mas info: https://es.wikipedia.org/wiki/Gast%C3%B3n_Soublette

** Ingresar término de busqueda en youtube
- Ingresamos a *https://www.youtube.com* e insertamos los siguientes términos de búsqueda:
 - gaston soublette
 - entrevista gaston soublette
 - entrevista a gaston soublette

- Estamos buscando una serie de entrevistas a Gaston Soublette
 - Tratamos de buscar y precisar la busqueda hasta encontrar alguna lista de reproducción
 - En su defecto, debemos buscar en el canal donde han subido los videos 
 - Esto es dificil por la falta de estructura de quien subió los archivos

** Primer resultado
Notamos que no encontramos todas las partes de manera secuencial en las primeras respuestas
[[file:./img/03.png]]

** Segundo resultado
Los dos primeros videos son partes secuenciales de la misma serie de videos
[[file:./img/04.png]]

** Tercer resultado
Notamos que encontramos una lista de reproducción con todos los videos de la serie en el cuarto puesto
[[file:./img/05.png]]

** Descargar videos
*** Obtención de links
- La sintaxis del link limpio se encuentra en la forma:
http://wwww.youtube.com/watch?v=Identificador_video

- La lista tiene adicionado el query *list*
&list=Identificador_lista

*** Descargar links de ejemplo
- Entrevista a Gastón Soublette - Parte I: La Sabiduría Tradicional
#+BEGIN_SRC bash
youtube-dl https://www.youtube.com/watch?v=ac9pat0lrwY
#+END_SRC

- Lista de reproducción: Entrevista a Gastón Soublette
#+BEGIN_SRC bash
youtube-dl https://www.youtube.com/watch?v=ac9pat0lrwY&list=PLLrDHs5ACtgGzUBWjCEicw9UCCYBi8GOm
#+END_SRC

** Algunos comandos de youtube-dl
Revisar el manual: 
#+BEGIN_SRC bash
man youtube-dl
#+END_SRC

- opciones generales: --list-extractor, --extractor-descriptions
- opciones de red
- restricciones geograficas
- selección de video: 
 - --playlist-start NUMBER, --playlist-end NUMBER
 - --match-title REGEX, --reject-title REGEX
 - --no-playlist, --yes-playlist
 - --include-ads
- opciones de descarga
 - --playlist-reverse, --playlist-random
- opciones de sistema de archivos
 - -a, --batch-file
- opciones de formato de video: 
 - -F, --list-formats
 - -f, --format FORMAT
- opciones de postprocesamiento: -x, --extract-audio

** Descargar desde un archivo
- Genera un archivo con links de canciones, llámalo */archivo/*
- Descargar videos dentro de una lista en un archivo, con alguna de las siguientes opciones:
#+BEGIN_SRC bash
youtube-dl -a archivo
youtube-dl --batch-file archivo
cat archivo | xargs youtube-dl
#+END_SRC

** Extraer audio de una lista de reproducción
- Buscar opcion para extraer audio de los videos
#+BEGIN_SRC bash
man youtube-dl | grep extract-audio
youtube-dl --help | grep extract-audio
#+END_SRC

- Extraer audio de la lista previa (Ambas opciones son exactamente iguales)
#+BEGIN_SRC bash
cat archivo | xargs youtube-dl --extract-audio --audio-format mp3
cat archivo | xargs youtube-dl -x --audio-format mp3
#+END_SRC

* Extraer audio de una lista de reproduccion 
** Limpiando un vínculo/URL
- Por ejemplo, al buscar por "/violeta parra/" en el buscador de youtube, encontra una lista con mas de 100 videos llamada "/Mix: Violeta parra/"

- Si clickeamos en ella y copiamos la dirección, obtendremos el siguiente link:
https://www.youtube.com/watch?v=w67-hlaUSIs&list=RDEMByAOZlKt4zL3wft7va81LA&start_radio=1

- El cual hay que limpiar, dejandolo de la siguiente forma:
https://www.youtube.com/playlist?list=ID_del_lista

- Obteniendo: 
https://www.youtube.com/playlist?list=RDEMByAOZlKt4zL3wft7va81LA

- Si no limpiamos la URL, nos descargará el primer video marcado en el query */watch?v=ID_del_video/*

** Descargando playlist con youtube-dl
- Luego pasandola a youtube-dl:
#+BEGIN_SRC bash
youtube-dl -x --audio-format mp3 --playlist-start 2 --playlist-end 3 https://www.youtube.com/playlist?list=RDEMByAOZlKt4zL3wft7va81LA
#+END_SRC

- De vuelta nos descargará el segundo y tercer video:
#+BEGIN_SRC bash
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 1 of Youtube mix
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 2 of Youtube mix
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 3 of Youtube mix
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 4 of Youtube mix
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 5 of Youtube mix
[youtube:playlist] RDEMByAOZlKt4zL3wft7va81LA: Downloading page 6 of Youtube mix
[download] Downloading playlist: Mix - Violeta Parra
[youtube:playlist] playlist Mix - Violeta Parra: Collected 119 video ids (downloading 2 of them)
[download] Downloading video 1 of 2
[youtube] sJxDb-z3fO0: Downloading webpage
[youtube] sJxDb-z3fO0: Downloading video info webpage
[download] Destination: Violeta Parra - Paloma Ausente-sJxDb-z3fO0.webm
[download] 100% of 3.18MiB in 00:00
[ffmpeg] Destination: Violeta Parra - Paloma Ausente-sJxDb-z3fO0.mp3
Deleting original file Violeta Parra - Paloma Ausente-sJxDb-z3fO0.webm (pass -k to keep)
[download] Downloading video 2 of 2
[youtube] F2ldR1EvhG0: Downloading webpage
[youtube] F2ldR1EvhG0: Downloading video info webpage
[download] Destination: Violeta Parra 'Miren como sonríen'-F2ldR1EvhG0.webm
[download] 100% of 2.42MiB in 00:00
[ffmpeg] Destination: Violeta Parra 'Miren como sonríen'-F2ldR1EvhG0.mp3
Deleting original file Violeta Parra 'Miren como sonríen'-F2ldR1EvhG0.webm (pass -k to keep)
[download] Finished downloading playlist: Mix - Violeta Parra
#+END_SRC

* Editar audio con ffmpeg 
** Introducción a ffmpeg
- La transformación a mp3 que realiza youtube-dl, en realidad es realizada por ffmpeg como se puede ver en el log anterior.

- El programa ffmpeg sirve para manipular audio y video con una multitud de opciones.

- Para instalar ffmpeg, ejecutamos en bash
#+BEGIN_SRC bash
sudo apt install ffmpeg
#+END_SRC

- Página oficial:
 https://www.ffmpeg.org/
- Documentación, lista de herramientas disponibles, componentes, API, etc:
 https://www.ffmpeg.org/documentation.html
- Documentación básica de ffmpeg:
 https://www.ffmpeg.org/ffmpeg.html

** Documentacion completa de ffmpeg e información adicional
- Documentación completa de ffmpeg:
 https://www.ffmpeg.org/ffmpeg-all.html

*** Nota para formato mp4
Las patentes de software llevaron a Debian / Ubuntu a deshabilitar los codificadores H.264 y AAC en ffmpeg.
Consulte /usr/share/doc/ffmpeg/README.Debian.gz.

La licencia de libfdk_aac no es compatible con GPL, por lo que la GPL no permite la distribución de archivos 
binarios que contengan código incompatible cuando también se incluye el código con licencia GPL. 
Por lo tanto, este codificador se ha designado como "no libre" y no puede descargar un ffmpeg 
precompilado que lo admita. Esto se puede resolver compilando ffmpeg por usted mismo.
Fuente: https://trac.ffmpeg.org/wiki/Encode/AAC

- Por lo tanto una *solucion* factible sería *usar formatos libres*

** Recortar audio con ffmpeg
- Descargar audio de la canción: Locko Way - Revancha!:
- Revisar los formatos disponibles con la opción -F:

#+BEGIN_SRC bash
youtube-dl -F https://www.youtube.com/watch?v=QqvTg1NtmI8

[youtube] QqvTg1NtmI8: Downloading webpage
[youtube] QqvTg1NtmI8: Downloading video info webpage
[youtube] QqvTg1NtmI8: Downloading MPD manifest
[info] Available formats for QqvTg1NtmI8:
format code  extension  resolution note
139          m4a        audio only DASH audio   49k , m4a_dash container, mp4a.40.5@ 48k (22050Hz)
140          m4a        audio only DASH audio  129k , m4a_dash container, mp4a.40.2@128k (44100Hz)
251          webm       audio only DASH audio  159k , webm_dash container, opus @160k (48000Hz)
278          webm       192x144    DASH video   95k , webm_dash container, vp9, 30fps, video only
160          mp4        192x144    DASH video  108k , mp4_dash container, avc1.4d400b, 30fps, video only
134          mp4        480x360    DASH video  118k , mp4_dash container, avc1.4d401e, 30fps, video only
242          webm       320x240    DASH video  220k , webm_dash container, vp9, 30fps, video only
135          mp4        640x480    DASH video  220k , mp4_dash container, avc1.4d401e, 30fps, video only
133          mp4        320x240    DASH video  242k , mp4_dash container, avc1.4d400c, 30fps, video only
243          webm       480x360    DASH video  405k , webm_dash container, vp9, 30fps, video only
244          webm       640x480    DASH video  752k , webm_dash container, vp9, 30fps, video only
18           mp4        480x360    360p  356k , avc1.42001E, mp4a.40.2@ 96k (44100Hz), 4.92MiB (best)
#+END_SRC

** Recortar audio con ffmpeg
- Descargar alguna version de formato libre (webm, opus)
- En nuestro caso usamos /format code 251/ (-f 251):
#+BEGIN_SRC bash
youtube-dl -f 251 https://www.youtube.com/watch?v=QqvTg1NtmI8
#+END_SRC

- Renombrar a un texto mas procesable para nosotros
#+BEGIN_SRC bash
mv Locko\ Way\ -\ 08\ -\ Revancha\!\ \(En\ memoria\ de\ Claudio\ Pozo\ a.k.a.\ BUXACA\)-QqvTg1NtmI8.webm revancha.webm
#+END_SRC

- Transcodificar audio con ffmpeg a /ogg audio/
#+BEGIN_SRC bash
ffmpeg -i revancha.webm -vn -acodec copy "revancha.oga"
#+END_SRC

** Recortar audio con ffmpeg
- Recortar audio con las siguientes especificaciones:
#+BEGIN_SRC bash
ffmpeg -ss 58 -t 31.50 -i revancha.oga -af 'afade=in:st=0:d=1,afade=out:st=31.10:d=0.40' revancha_salida.mp3
ffmpeg -ss 58 -t 31.50 -i revancha.oga -af 'afade=in:st=0:d=1,afade=out:st=31.10:d=0.40' revancha_salida.oga
#+END_SRC

- Reproducir archivo final
#+BEGIN_SRC bash
mpv revancha.mp3
#+END_SRC

** Detalle de la sintaxis de ffmpeg
- ss :: busca el tiempo de inicio del audio en segundos (seek stream)
- -t :: tiempo de duracion del recorte de audio - para recortes cortos (time)
- -i :: archivo de entrada (input)
- -af :: formato de audio (audio format)
- afade :: difuminado de audio (audio fade)
- afade=in :: difuminado de entrada (audio fade in)
- afade=out :: difuminado de salida (audio fade out)
- st :: tiempo de inicio (starting time)
- d :: tiempo de duracion del difuminado (duration)

* Otras formas de descargar
** Redes centralizadas, descentralizadas y distribuidas
Diferentes arquitecturas de red:
file:./img/06.png
** La nube no existe
Esto no nos dice nada:
[[file:./img/07.jpg]]
** La nube no existe
Esto nos dice algo, pero no busca enseñar/ilustrar:
[[file:./img/08.jpg]]
** Red centralizada: Servidor web
El servidor web estandar es un ejemplo de una red centralizada. Con el servidor al centro y los clientes alrededor.

Cuando descargamos de youtube, estamos descargando de un servidor centralizado (en general)
[[file:./img/09.jpg]]
** Red distribuida: CDN - Content Delivery Network: Red de distribucion de contenido
El CDN distribuye la carga de las solicitudes de los clientes al servidor central, enrutando el trafico del cliente 
a los centros de datos locales mas cercanos.

Los archivos almacenados en telegram están en varias CDN: https://core.telegram.org/cdn
[[file:./img/10.png]]
** Red descentralizada: P2P - Peer to Peer - compañero a compañero - par a par
El contenido se distribuye entre todos los que lo comienzan a descargar y quienes ya poseen copia completa de lo descargado.
El cliente se transforma en servidor una vez terminada la descarga, alimentando la red en un circulo virtuoso de
personas compartiendo archivos.
[[file:./img/11.jpg]]
** P2P: Tracker, Seeder y Leecher
- Tracker - Rastreadores :: Servidor que contiene la información para que los pares se conecten unos con otros. 
Llevan registro de qué usuarios contienen el archivo que se quiere descargar.
- Seeder - Semillas :: Usuarios que poseen el archivo completo.
- Leecher - Sanguijuelas :: Usuarios que están descargando el archivo, pero que todavía no tienen el archivo completo.
Cuando la descarga termina, se pasa a ser seeder. La denominacion despectiva proviene de que hay usuarios que descagan y luego no continuan compartiendo lo que han descargado.
- Swarm - Enjambre :: El conjunto de usuarios tanto seeders como leechers que conforman el ecosistema P2P y que pululan el tracker.
- Peer :: Se le llama al usuario individual, independiente de si es seeder o leecher.

- Mas info: https://es.wikipedia.org/wiki/BitTorrent

** Programas P2P
- qbittorrent :: Cliente protocolo bittorrent (similar utorrent/windows)
- amule :: Cliente de protocolo edonkey (similar emule/windows).
- soulseek :: No es software libre, no tiene publicidad, ni malware. Depende de un servidor centralizado y es desarrollado por un ex-desarrollador de napster.
- nicotine :: Cliente libre de soulseek. No se recomienda su uso debido a que es confuso su uso por su interfaz gráfica poco desarrollada.
- fopnu :: Cliente libre para la red P2P de fopnu
- Demonsaw :: Cliente para la red P2P de demonsaw
- GNUnet :: Programa para compartir archivos en la GNUnet

*** Instalar programas
#+BEGIN_SRC bash
sudo apt install qbittorrent amule fopnu gnunet nicotine
#+END_SRC

*** Nota
fopnu no requiere mayor configuración

** Configurar Qbittorrent
- Iniciar qbittorrent
- Ir al menú /Ver/ y activar la casilla /Motor de búsqueda/
- Apareceŕa la pestaña de búsqueda, la seleccionamos
- Abajo a la derecha clickeamos el botón llamado /Plugins de búsqueda/
- Abrirá una ventana para gestionar los plugins, clickeamos en el boton /Buscar Actualización/
- Esperamos a que actualice los plugins y cerramos dicha ventana
- Ya podemos usar la barra de búsqueda

** Configurar Amule
- Arrancar amule, al iniciar se autoconfiguran los servidores
  - Conectar con un servidor y buscar lo deseado.
*** Configurar servidores adicionales
- Buscar /servidores emule 2019/ en tu buscador favorito, encontrarás paginas como estas:
 - https://forum.emule-project.net/index.php?showtopic=155071
 - https://www.adslzone.net/foro/emule.49/servidores-emule-2019-verdaderos.55732/
- Revisando la primera página, encontramos:
 - lista de servidores
 - /servidores .met/
*** Para configuraciones avanzadas
Es necesario consultar la especificacion de los archivos usados por amule:
http://wiki.amule.org/wiki/AMule_files
** Configurar servidores amule manualmente
- En amule, en la pestaña /Redes/, ingresamos estos 3 datos manualmente y apretamos boton /Añadir/
#+BEGIN_SRC bash
Nombre               IP               Puerto del servidor
TV Underground       176.103.48.36    4184
GrupoTS.net Server   46.105.126.71    4661
eDonkey server No1   176.103.50.225   8369
eDonkey server No2   176.103.56.135   2442
eDonkey server No3   91.226.212.11    2442
eMule Security       80.208.228.241   8593
#+END_SRC

- Se agregarán los servidores a la lista
- Conectar a servidor deseado (Aquel con mas usuarios o mas archivos):
 - Doble click en el servidor, o alternativamente
 - Boton secundario y seleccionando la opción /Conectar al servidor/
** Instalar Soulseek
Seguimos los mismos pasos que para descargar youtube-dl
- Ingresamos a la página de soulseek: http://slsknet.org/
- Apretamos en /Download/ en el menu derecho en la pág. web: http://www.slsknet.org/news/node/1
- Copiamos el link de /Latest Linux 64-bit AppImage: SoulseekQt-2018-1-30-64bit-appimge.tgz/
- En la terminal ejecutamos
#+BEGIN_SRC bash
wget https://www.slsknet.org/SoulseekQt/Linux/SoulseekQt-2018-1-30-64bit-appimage.tgz
tar xzvf SoulseekQt-2018-1-30-64bit-appimage.tgz
sudo mv SoulseekQt-2018-1-30-64bit.AppImage /usr/local/bin/soulseek
sudo chmod a+rx /usr/local/bin/soulseek
#+END_SRC

- Ya lo podemos ejecutar desde la terminal con:
#+BEGIN_SRC bash
soulseek
#+END_SRC

*** Nota 
Soulseek exige registrar un nombre de usuario y una contraseña (sin mail)
para poder ser contactado en futuros inicios de sesión

** Instalar Demonsaw 
Seguimos los mismos pasos que para descargar youtube-dl
- Ingresamos a la página de demonsaw: https://www.demonsaw.com/
- Clickeamos en /Download/ en el apartado /Linux: Compatible with Debian 8+ and most 64 bit GNU derivatives/
- Descargará /demonsaw_nix_64.tar.gz/. Lo extraemos por terminal y lo ejecutamos
- En la terminal:
#+BEGIN_SRC bash
tar xzvf demonsaw_nix_64.tar.gz
cd demonsaw_nix_64/
./Demonsaw
#+END_SRC

- Podemos mantener el nick generado al azar y el /router/ por defecto
- Clickea en /Start/
- Ya puedes revisar:
 - lo que se comparte en la pestaña /Browse/
 - chatear en la pestaña /chat/
 - compartir archivos en la pestaña /share/

* Páginas para descargar
** Páginas para descargar documentales
- Docuwiki :: Wiki de documentales en inglés, en descarga directa por la red edonkey (emule/amule).
 http://docuwiki.net/
- Documania :: Página para encontrar documentales en español y latino.
 https://www.documaniatv.com/
- Subdivx :: Foro latinoamericano para buscar subtitulos de series, peliculas, documentales, etc.
 http://subdivx.com/
- Argenteam :: Foro argentino para descargar sibtitulos de series, peliculas, documentales, etc.
 https://argenteam.net/

* Tarea
** Laboratorio de búsqueda
Objetivo: Encontrar el siguiente documental doblado al español.
- Autora: Cosima Dannoritzer
- Título en inglés: Time Thieves
- Título en español: Ladrones de tiempo
- Tema: Discute respecto a que lo último que le quedaba al capitalismo por robarnos era el tiempo

Aplicar todo lo aprendido en clase.

** Información adicional
Esta información no es relevante para encontrar el documental doblado al español, pero proporciona mas antecedentes:
- Trailer: https://www.youtube.com/watch?v=dwLETJjVemw
- Transmisión en TV3 de cataluña en catalan:
https://www.ccma.cat/tv3/alacarta/sense-ficcio/lladres-de-temps/video/5801763/
- Transmisión en RTVE en español:
http://www.rtve.es/alacarta/videos/otros-documentales/otros-documentales-ladrones-del-tiempo/5367984/
- Version en catalan:
https://www.youtube.com/watch?v=_LhxjlKMy2Q
- Página oficial del film:
http://www.timethievesfilm.com/es/

** Solución
Se puede descargar de la red edonkey (amule), en el servidor TV-Underground,
bajo el nombre de /Otros documentales - Ladrones del tiempo.mp4/

- URL magnético:
magnet:?dn=Otros%20documentales%20-%20Ladrones%20del%20tiempo.mp4&xt=urn:ed2k:f4268230781baa79c65d5e637de30e01&xt=urn:ed2khash:f4268230781baa79c65d5e637de30e01&xl=423599249

Busqué sin exito en:
- fopnu
- soulseek
- qbittorrent, en el metabuscador integrado en el programa
- docuwiki
- documania

* Buscar alternativas
** Otros programas de edicion de audio, video, etc.
- Aegisub :: Programa para edición de subtitulos:
http://www.aegisub.org/ 

- Kdenlive :: Programa recomendado para edición de video:
https://kdenlive.org/es/

- Audacity :: Programa recomendado para grabación y edición de audio básica:
https://www.audacityteam.org/

- Ardour :: Programa recomendado para producción de audio:
https://ardour.org/

** Instalar programas y buscar alternativas
*** Instalar los programas anteriores
- Estos programas se pueden instalar con su nombre:
#+BEGIN_SRC bash
sudo apt install aegisub audacity ardour kdenlive
#+END_SRC

- Si no sabes como se llaman, puedes buscar en apt con:
#+BEGIN_SRC bash
apt search aegisub
#+END_SRC

*** Buscar alternativas
Para buscar alternativas a los programas anteriores en la plataforma gnu/linux, podemos usar:
http://alternativeto.net/


** Tarea para la casa
Investigar como funciona GNUnet y enseñarnos a todos!

* Referencias
** Páginas de referencia consultadas

- chown: https://www.computerhope.com/unix/uchown.htm
- chmod: https://www.computerhope.com/unix/uchmod.htm
- youtube-dl: https://askubuntu.com/questions/486297/how-to-select-video-quality-from-youtube-dl
- ffmpeg: 
 - https://superuser.com/questions/412890/lossless-extraction-of-streams-from-webm
 - https://stackoverflow.com/questions/20295398/ffmpeg-clip-audio-interval-with-starting-and-end-time
 - https://video.stackexchange.com/questions/19867/how-to-fade-in-out-a-video-audio-clip-with-unknown-duration
 - https://ffmpeg.org/ffmpeg-filters.html#afade-1
