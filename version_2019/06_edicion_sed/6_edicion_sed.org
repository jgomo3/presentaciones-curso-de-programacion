#+TITLE: Textos en Bash, conversión  y edición con sed
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+BEAMER_HEADER: \setbeamertemplate{navigation symbols}{}
#+COLUMNS: %5ITEM %10BEAMER_ENV(Env) %5BEAMER_ACT(Act) %6BEAMER_COL(Col)
#+latex_class_options: [10pt]
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing
#+LaTeX_CLASS_OPTIONS: [aspectratio=169]
#+BEAMER_HEADER: \definecolor{links}{HTML}{0000A0}
#+BEAMER_HEADER: \hypersetup{colorlinks=,linkcolor=,urlcolor=links}
#+BEAMER_HEADER: \setbeamertemplate{itemize items}[default]
#+BEAMER_HEADER: \setbeamertemplate{enumerate items}[default]
#+BEAMER_HEADER: \setbeamertemplate{items}[default]
#+BEAMER_HEADER: \setbeamercolor*{local structure}{fg=darkred}
#+BEAMER_HEADER: \setlength{\parskip}{\smallskipamount}


* Temario de la clase

** ¿Qué veremos en esta clase?

- Aprenderemos a transformar archivos pdf a texto :: con el fin de poder buscar
     y extraer la información que contienen.

- Aprenderemos a seleccionar y transformar cadenas de texto :: dentro de los
     archivos y streams, utilizando el comando ~sed~ y las acciones disponibles. 

* Registrar inicio de clases
** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *edición_sed*.

#+BEGIN_SRC bash
echo "CLASE_06::edicion_sed"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_06::edicion_sed*


* ¿Cómo seleccionamos y detectamos patrones en un texto?

** Estudiando relaciones entre distintos textos

En esta clase tomaremos algunos textos que hablan sobre la crisis del planeta que sufre
a partir de los modos de explotación capitalista, tomaremos cuatro discursos o escritos de 
personajes históricos que han aportado con su visión y trabajo.

[[file:./img/grupo_discursos.svg.png]]

** Estudio del discurso de Greta

A fines de septiembre *Greta Thunberg*, la joven activista asistió a la ONU 
a presentar un discurso, en la línea con todos sus llamados y [[https://www.fridaysforfuture.org/greta-speeches][discursos]] desde
que se ha transformado en un personaje público. 

- ¿Cómo se atreven? :: https://www.youtube.com/watch?v=EZ9DWQKqDW0

Vamos a leer el discurso en /castellano/ en el archivo 
*./ejercicios/discursos/greta_onu_2019.txt*

** Anotar marcas en el texto

Leyendo el texto y usando ~grep~, anotar en tus apuntes las líneas 
que contengan, y el contexto:

- Ecosistema
- Clima
- Mundo
- Mal
- Números, valores, datos

* Convertir pdf a texto

** Cuando el texto está en pdf

Vamos a instalar las utilidades de ~poppler~, que es un conjunto de herramientas
para la manipulación de ~pdf~ y conversión a diferentes formatos. 

Nuestro objetivo es /simplificar/ el acceso al texto obteniendo un archivo de texto
*ASCII*

#+BEGIN_SRC bash
sudo apt-get install poppler-utils
#+END_SRC

** Convierte pdf a txt

Vamos a convertir un archivo que contiene texto ~pdf~ a un archivo de 
texto, de manera que podamos acceder y manipular su contenido de manera
programática.

Tenemos el archivo *manifiesto_comunista.pdf* y lo convertiremos a un archivo
de texto.

#+BEGIN_SRC bash
pdftotext manifiesto_comunista.pdf manifiesto_comunista.txt
#+END_SRC

Abrimos el archivo y vemos que esté bien formateado, de lo contrario, hacemos lo que
sucede en la siguiente diapositiva.

* Ajustar texto a un tamaño de columna

** Obtener un texto desde la web

Vamos a buscar el *Discurso de Salvador Allende en la ONU*,

- url :: http://www.abacq.net/imagineria/cronolo4.htm
- copiar el texto
- pegar un un archivo *salvador_allende_onu.txt*

Observamos que cada párrafo tiene una línea muy extensa a la derecha.

** Ajustar un texto para lectura en una pantalla


El texto descargado y copiado a un archivo necesita un poco más de trabajo, por
lo que será necesario ajustar la cantidad de columnas con texto disponible para 
dar una mayor legibilidad.

#+BEGIN_SRC bash
fold -w 80 -s salvador_allende_onu.txt \
> salvador_allende_onu_final.txt
#+END_SRC

De esa manera, obtenemos un texto presentable, ajustado a *80 caracteres*.

* Tarea para la casa: búsqueda y selección

Se ha creado una misión muy importante, es la *Tarea de búsqueda y selección*
para resolver grupalmente.

Se ubica en el archivo *tarea_grep_sed.org* y describe los ejercicios a realizar
con los textos de:

- Greta Thunberg
- Salvador Allende
- Karl Marx
- Elinor Ostrom

Cada uno de ellos es un tanto distinto en sus formatos de presentación y
contenidos, pero están relacionados. La misión consiste en encontrar la relación
utilizando las herramientas aprendidas hasta ahora en el curso.

¡Éxito!

* Una herramienta que nos ayuda a seleccionar y modificar

** El comando ~sed~ para editar en la terminal

El comando ~sed~ es parte de la *trifuerza POSIX* y nos ayudará a seleccionar
textos desde /archivos o streams/, para eso debemos tener en cuenta que existen
en los textos:

- marcas
- números de línea
- patrones de texto 
- frases especiales 

Con eso, podemos controlar que ver o no, que modificar o no. Para ubicarnos
siempre será de ayuda ~grep~ y los editores de texto.

#+BEGIN_SRC bash
man sed
#+END_SRC

** Uso de sed tomando un archivo

La estructura para usar ~sed~ y *leer un archivo* es la siguiente 

#+BEGIN_EXAMPLE
sed [OPCIONES] '{comandos sed}' ARCHIVO(s)
#+END_EXAMPLE

En dónde la parte *{comandos sed}* puede ser una o mas acciones
que se realizan sobre cada línea que se lea.

- '1 comando sed'
- '{comando sed 1; comando sed 2}'

** Uso de sed tomando un stream

De manera análoga, tomando desde un ~stream~ o ~salida estándar~.

#+BEGIN_EXAMPLE
cat ARCHIVO| sed [OPCIONES] '{comandos sed}'
#+END_EXAMPLE

** Cuando los comandos son varios, hacer script

En caso que la cantidad de ~comandos sed~ sean muchas, es muy recomendable
hacer un ~script sed~.

#+BEGIN_SRC bash
nano comandos.sed
#+END_SRC

#+BEGIN_EXAMPLE
comando sed 1;
comando sed 2;
comando sed 3;
#+END_EXAMPLE

Y se ejecuta el comando /general de sed/ así:

#+BEGIN_EXAMPLE
sed [OPCIONES] -f comandos.sed ARCHIVO(s)
#+END_EXAMPLE

* Aprender lo básico de sed

** Mostrar lo que se lee

Utilizando el archivo *ejercicios/texto1*, realizaremos las siguientes
pruebas de concepto para ver que tan /útil/ es la herramienta.

Imprimir la misma línea que entra como salida (comando  *p*)

#+BEGIN_SRC bash
sed 'p' texto1
#+END_SRC

Omitir mostrar solo la salida (opción *-n*)

#+BEGIN_SRC bash
sed -n 'p' texto1
#+END_SRC

** Seleccionar líneas

Mostrar solo una línea *N=4*

#+BEGIN_SRC bash
sed -n '4 p' texto1
#+END_SRC

Mostrar la línea *N=4* a *M=8*

#+BEGIN_SRC bash
sed -n '4,8 p' texto1
#+END_SRC

Desde *N=4* hasta el final (uso de *$*)

#+BEGIN_SRC bash
sed -n '4,$ p' texto1
#+END_SRC

** Mostrar solo cada tantas líneas

Para mostrar solo aquellas líneas pares, impares o cada algún número
especial se usa el símbolo *~* llamado *cola de chancho* en Chile.

#+BEGIN_SRC bash
# impares
sed -n '1~2 p' texto1
# pares
sed -n '2~2 p' texto1
# cada 4 desde 3
sed -n '3~4 p' texto1
#+END_SRC

** Uso de patrones de texto

De manera análoga a seleccionar ciertas líneas específicas, podemos seleccionar
solamente aquellas líneas que contienen coincidencia con un patrón de texto.

#+BEGIN_SRC bash
sed -n '/Anna/ p' texto1
sed -n '/Pedro/ p' texto1
#+END_SRC

** Mostrar entre dos patrones de texto 

Desde que aparece /Anna/ hasta que aparece /Pedro/

#+BEGIN_SRC bash
sed -n '/Anna/,/Pedro/ p' texto1
#+END_SRC

¿Qué líneas hay entre medio?

** Mostrar las siguientes N líneas después de coincidir.

Si *N=4*, después de /Anna/ debería mostrar 4 líneas.

#+BEGIN_SRC bash
sed -n '/Anna/,+4 p' texto1
#+END_SRC

* Eliminar el texto bajo ciertas marcas, comando 'd'

** Borrar líneas con una coincidencia

Deseamos borrar todas las líneas que contengan /Pedro/, el uso de ~d~
al final nos permitirá filtrar o descartar la línea con esa coincidencia.

#+BEGIN_SRC bash
sed '/Pedro/ d' texto1
#+END_SRC

** Borrar líneas N a M

De manera análoga, para no mostrar desde línea *N a M*

#+BEGIN_SRC bash
sed 'N,M d' texto1
#+END_SRC

** Descartar o borrar líneas vacías.

Esta es muy útil, ya que suele utilizarse para limpiar archivos de líneas en blanco.

#+BEGIN_SRC bash
sed '/^$/ d' texto1
#+END_SRC

Utiliza una ~regex~ que dice que desde el inicio (^) a final ($) no hay nada.

* Escribir resultados a un archivo, comando 'w'

** Guardar las acciones de sed en un archivo

De manera interna ~sed~ dispone de la posibilidad de escribir los resultados de 
un comando a otro archivo, para eso se usa la estructura

#+BEGIN_EXAMPLE
comando_sed w archivo_salida.txt
#+END_EXAMPLE

De la manera siguiente, escribe todas las coincidencias de /Pedro/ a *otro_archivo*:

#+BEGIN_SRC bash
sed '/Pedro/ w otro_archivo' texto1
#+END_SRC

* Modificar el texto bajo ciertas marcas

** Un uso muy común de sed, cambiar cosas

Cuando necesitamos cambiar todas coincidencias de un texto, debemos considerar
lo siguiente.

- La lectura línea a línea de sed :: por cada línea que entra a ~sed~ se ejecuta
     el o los comandos.

- El conteo de coincidencias :: por cada coincidencias se identifica con una
     posición.

** Cambiar solo la primera coincidencia de cada línea 

Cambiar Pedro por Peter

#+BEGIN_SRC bash
sed 's/Pedro/Peter/' texto1
#+END_SRC

Cambiar sueño por visión

#+BEGIN_SRC bash
sed 's/sueño/visión/' texto1
#+END_SRC

** Cambiar todo usando 'g'


Cambiar Pedro por Peter

#+BEGIN_SRC bash
sed 's/Pedro/Peter/g' texto1
#+END_SRC


** Mostrar solo los cambios 


Cambiar Pedro por Peter

#+BEGIN_SRC bash
sed -n 's/Pedro/Peter/g p' texto1
#+END_SRC


** Cambiar la coincidencia en la posición 'N'


Cambiar Pedro por Peter,  *N=2*

#+BEGIN_SRC bash
sed 's/Pedro/Peter/2' texto1
#+END_SRC

** Ignorar mayúsculas de minúsculas

Compara, es sensible a tamaño de letra

#+BEGIN_SRC bash
sed -n 's/sueño/visión/gp' texto1
#+END_SRC

Con

#+BEGIN_SRC bash
sed -n 's/sueño/visión/gip' texto1
#+END_SRC

** Seleccionar entre N y M, cambiar y guardar 

Una combinación interesante, para las coincidencias de Pedro, entre 14 y 19,
guardar en archivo pedro_14_19.txt'

#+BEGIN_SRC bash
sed -n '14,19 s/Pedro/Peter/giw pedro_14_19.txt' texto1
#+END_SRC

* Intuición de un comando sed
** Usando la imaginación

En base a lo aprendido, la estructura general de un comando sed podría
comprenderse de la siguiente manera.

[[file:./img/intuicion.png]]

* Escribir varias acciones de tipo sed en un script
** Editar y crear nuevo archivo

Cualquier acción de ~sed~ sobre la que se cambie algo, puede registrarse
completa en la salida estándar (sin ocultar las líneas no modificadas)

#+BEGIN_SRC bash
sed 's/Pedro/Peter/g' texto1 > cambios_pedro
#+END_SRC

Luego, revisa si existe el contenido.

#+BEGIN_SRC bash
cat cambios_pedro
#+END_SRC

** Editar mismo archivo, usando la opción '-i'

Cualquier acción de ~sed~ sobre la que se cambie algo, puede registrarse
completamente y cambiar en el mismo archivo. A veces es recomendable.

#+BEGIN_SRC bash
cp texto1 texto2
sed -i 's/Pedro/Peter/g' texto2
#+END_SRC

Luego, revisa si existe el contenido.

#+BEGIN_SRC bash
cat texto2
#+END_SRC

* El uso de expresiones regulares

** Los patrones de texto SON expresiones regulares

Tomar el archivo *ejercicios/primer_regex.txt* y modificar 

- mamá -> superior 
- papá -> superior
- eliminar líneas vacías

Es decir, tomar la *expresion regular* que agrupa ambas 

#+BEGIN_EXAMPLE
[pm]a[pm]á
#+END_EXAMPLE

y reemplazarlas por *superior*, utilizando ~sed~. Además, incluir el comando 
que borre las líneas en blanco 

#+BEGIN_EXAMPLE
'/^$/ d'
#+END_EXAMPLE

* El conjunto de herramientas

** La trifuerza POSIX

Dentro de todo el conjunto de herramientas disponibles del sistema
*POSIX*, lo podemos entender como la *trifuerza* esencial con
{~grep~, ~sed~, ~awk~} y el uso de las ~expresiones regulares~.

#+NAME:   fig: trifuerza_posix
#+ATTR_HTML: width="150px"
#+ATTR_ORG: :width 150
#+attr_latex: :width 150px
[[file:./img/trifuerza.png]]

* Material de referencia

** Lecturas a estudiar sobre nuestros personajes

- Manifiesto Comunista :: Karl Marx 
- Discurso en la ONU :: Salvador Allende
- El gobierno de los bienes comunes :: Elinor Ostrom
- Genes, Bytes y Emisiones :: Silke Hellfrish
- Discurso en la ONU :: Greta Thungberg

** Sobre sed

- Buscador Pirata Library Gen Ru :: http://gen.lib.rus.ec
- sed & awk :: Dale Dougherty, Arnold Robbins
- sed & awk :: Pocket Reference
- sed & awk :: Ramesh Natarajan
