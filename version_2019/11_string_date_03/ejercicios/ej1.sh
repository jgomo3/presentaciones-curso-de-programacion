#! /bin/bash

read -p "Dame una frase: " frase
read -p "Qué quieres buscar?: " buscar


echo "En la primera posición está <${frase:0:1}>"

# usando llamada a variable
mitad=$((${#frase}/2))
echo $mitad
echo "En la mitad está <${frase:mitad:1}>"

# usando llamada a variable
ultima_pos=$((${#frase}-1))
echo "En la última posición está <${frase:ultima_pos}>"

# usando bc
ultima_pos=$(echo ${#frase}-1|bc)
echo "En la última posición está <${frase:ultima_pos}>"

### Aca comenzamos  buscar la posicion de $buscar

recorte=${frase/$buscar*/}
posicion=${#recorte}
echo "La palabra $buscar está en la posición $posicion"
