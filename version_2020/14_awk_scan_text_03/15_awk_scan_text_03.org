#+LATEX_COMPILER: lualatex
#+TITLE: Lenguaje para el escaneo y procesamiento de AWK III
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing


* Temario de la clase

** ¿Qué veremos en esta clase?

- Lógica booleana :: Para evaluar proposiciones y conjuntos de ellas.

- Uso de operadores matemáticos :: Para realizar operaciones matemáticas y
     definir funciones que las necesiten.

- Crear funciones :: definir funciones para el uso eficiente de código.

- Crear librerías de funciones :: agrupar funciones en un archivo aparte y
     utilizarlo adecuadamente.

* Registrar inicio de clases

** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *awk_03*.

#+BEGIN_SRC bash
echo "CLASE_14:awk_03"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_14::awk_03*
     

* Uso control de flujo con ~if~
  
** Controlar el número de fila con NR.

Deseamos ver las exportaciones de vino al mundo desde chile. Estos datos están
en el archivo *./ejercicios/exp_vino/exp_vino.csv* y fueron descargados desde
*DataChile*

Primero, deseamos mostrar desde las filas NR=25 hasta NR=30.

#+NAME: vino_noif
#+BEGIN_SRC shell :results replace output  :session vino :exports both
awk -F',' '(NR>=25 && NR<=30){
            print NR,$0}' ./ejercicios/exp_vino/exp_vino.csv
#+END_SRC

Resulta en la selección de las líneas mencionadas.

#+RESULTS: vino_noif
:results:

> 25 2,África,108,Ghana,2014,2014,04,Foodstuffs,042204,Wine,249887.5
26 2,África,108,Ghana,2015,2015,04,Foodstuffs,042204,Wine,210793.1875
27 2,África,108,Ghana,2016,2016,04,Foodstuffs,042204,Wine,158571.3125
28 2,África,109,Togo,2011,2011,04,Foodstuffs,042204,Wine,77257.0
29 2,África,109,Togo,2012,2012,04,Foodstuffs,042204,Wine,107102.6015625
30 2,África,109,Togo,2013,2013,04,Foodstuffs,042204,Wine,56103.6015625
:end:

Utilizando ~if~

#+NAME: vino_if
#+BEGIN_SRC shell :results replace output  :session vino :exports both
  awk -F',' '{if(NR>=25 && NR<=30){
              print NR,$0}}' ./ejercicios/exp_vino/exp_vino.csv
#+END_SRC

Obtenemos el mismo resultado que si no se usara ~if~

#+RESULTS: vino_if
:results:

25 2,África,108,Ghana,2014,2014,04,Foodstuffs,042204,Wine,249887.5
26 2,África,108,Ghana,2015,2015,04,Foodstuffs,042204,Wine,210793.1875
27 2,África,108,Ghana,2016,2016,04,Foodstuffs,042204,Wine,158571.3125
28 2,África,109,Togo,2011,2011,04,Foodstuffs,042204,Wine,77257.0
29 2,África,109,Togo,2012,2012,04,Foodstuffs,042204,Wine,107102.6015625
30 2,África,109,Togo,2013,2013,04,Foodstuffs,042204,Wine,56103.6015625
:end:

Podemos decir que la primera forma es *implícita* y la segunda *explícita* pero
en ambas la estructura de control cumple la misma funcionalidad.

** Mayores detalles y opciones con NR

Luego, además, deseamos mostrar las filas NR=50 hasta NR=55.

Además, debemos mostrar la línea que se está mostrando.

#+NAME: vino_noif2
#+BEGIN_SRC shell :results replace output  :session vino :exports both
awk -F',' '(NR>=25 && NR<=30){
            print NR,$0}
           (NR>=50 && NR<=55){
            print NR,$0}' ./ejercicios/exp_vino/exp_vino.csv
#+END_SRC

Muestra las líneas solicitadas.

#+RESULTS: vino_noif2
:results:

> > 25 2,África,108,Ghana,2014,2014,04,Foodstuffs,042204,Wine,249887.5
26 2,África,108,Ghana,2015,2015,04,Foodstuffs,042204,Wine,210793.1875
27 2,África,108,Ghana,2016,2016,04,Foodstuffs,042204,Wine,158571.3125
28 2,África,109,Togo,2011,2011,04,Foodstuffs,042204,Wine,77257.0
29 2,África,109,Togo,2012,2012,04,Foodstuffs,042204,Wine,107102.6015625
30 2,África,109,Togo,2013,2013,04,Foodstuffs,042204,Wine,56103.6015625
50 2,África,117,Zambia,2012,2012,04,Foodstuffs,042204,Wine,131979.0
51 2,África,117,Zambia,2013,2013,04,Foodstuffs,042204,Wine,46013.0
52 2,África,117,Zambia,2014,2014,04,Foodstuffs,042204,Wine,91431.5390625
53 2,África,117,Zambia,2016,2016,04,Foodstuffs,042204,Wine,65197.78125
54 2,África,119,Mauricio,2011,2011,04,Foodstuffs,042204,Wine,369594.0
55 2,África,119,Mauricio,2012,2012,04,Foodstuffs,042204,Wine,355237.375
:end:

O bien, con if, entrega el mismo resultado:
 
#+NAME: vino_if2
#+BEGIN_SRC shell :results replace output  :session vino :exports both
awk -F',' '{if(NR>=25 && NR<=30){
            print NR,$0}
           else if (NR>=50 && NR<=55){
            print NR,$0}}' ./ejercicios/exp_vino/exp_vino.csv
#+END_SRC

#+RESULTS: vino_if2
:results:

> > 25 2,África,108,Ghana,2014,2014,04,Foodstuffs,042204,Wine,249887.5
26 2,África,108,Ghana,2015,2015,04,Foodstuffs,042204,Wine,210793.1875
27 2,África,108,Ghana,2016,2016,04,Foodstuffs,042204,Wine,158571.3125
28 2,África,109,Togo,2011,2011,04,Foodstuffs,042204,Wine,77257.0
29 2,África,109,Togo,2012,2012,04,Foodstuffs,042204,Wine,107102.6015625
30 2,África,109,Togo,2013,2013,04,Foodstuffs,042204,Wine,56103.6015625
50 2,África,117,Zambia,2012,2012,04,Foodstuffs,042204,Wine,131979.0
51 2,África,117,Zambia,2013,2013,04,Foodstuffs,042204,Wine,46013.0
52 2,África,117,Zambia,2014,2014,04,Foodstuffs,042204,Wine,91431.5390625
53 2,África,117,Zambia,2016,2016,04,Foodstuffs,042204,Wine,65197.78125
54 2,África,119,Mauricio,2011,2011,04,Foodstuffs,042204,Wine,369594.0
55 2,África,119,Mauricio,2012,2012,04,Foodstuffs,042204,Wine,355237.375
:end:

Es posible también, para ambos usos del filtro, considerar diferentes bloques.


* Uso de expresiones regulares y condicional

** Awk y las expresiones regulares

Finalmente, la combinación de ~awk~ con expresiones regulares permitirá tener un
control total sobre la información con que estamos trabajando. Provee, de esta
manera, las herramientas necesarias y suficientes para procesarla.

[[file:./img/awk_regex_navaja.png]]

** Coincidencia sobre la fila

Retomando el archivo de *Exportaciones de Vino*, si solo deseamos mostrar
aquellas relacionadas con /Europa/.

#+BEGIN_SRC bash
awk '($0 ~ /Europa/){print}' exp_vino.csv
#+END_SRC

** Coincidencia sobre el campo

Si deseamos mayor precisión, podemos abordar el problema desde los campos. En
este caso el *continente* corresponde al *campo 2*.

#+BEGIN_SRC bash
awk -F',' '($2~/Europa/){print}' exp_vino/exp_vino.csv
#+END_SRC

Si deseamos, además, filtrar aún aquellas exportaciones que ocurrieron el
año 2015. El campo del año es el *campo 5*.

#+BEGIN_SRC bash
awk -F',' '($2~/Europa/ && $5==2015){
            print}' exp_vino/exp_vino.csv
#+END_SRC

** Uso de la función ~match~

Como ejercicio, realizar los mismos ejemplos anteriores de *regex*, utilizando la función
*match*. Que es de la forma.

#+BEGIN_EXAMPLE
match(string, regex)
#+END_EXAMPLE

Por ejemplo.

#+BEGIN_SRC bash
awk -F',' '(match($2,/Europa/)){
            print $0}' exp_vino.csv
#+END_SRC


** Uso de regex en control de flujo ~if~

Nuevamente, realizar los ejercicios anteriores, pero utilizando explícitamente
~if~ en el programa. Es decir.

#+BEGIN_SRC bash
awk -F',' '{
if(match($2,/Europa/)){print $0}}' exp_vino.csv
#+END_SRC


* Uso de operadores matemáticos

  
** Operaciones matemáticas

- $a++$ $b--$      :: incremento de a o decremento de b en 1.

Ejemplo de restar:

#+BEGIN_SRC awk
  BEGIN{
  contador=0;
  print("Primer valor", contador);
  contador--;
  print("Segundo valor", contador);
  contador--;
  print("Tercer valor", contador);
  }
#+END_SRC

  Ejemplo de sumar:

#+BEGIN_SRC awk
  BEGIN{FS=","; europa=0}
  ($2 ~ /Europa/){
  print("Exportacion a Europa", "pais: "$5);
  europa++
  }
  END{
  print("La cantidad de exportaciones a Europa fue:" europa)
  }
#+END_SRC

Lo básico $a+b$, $a-b$,sumar o restar

Multiplicar o dividir $a*b$, $a/b$, o módulo $a\%b$.

Ejemplo de uso de módulo, buscar los números pares a partir de una lista de
  valores dados. La función *módulo %* entrega lo que sobra en enteros de dividr a
  con b.

#+BEGIN_SRC awk   
BEGIN{b=2}
{
if($0%b==0){
print $0" es par"
}else{
print $0" no es par"
}
}
#+END_SRC

Las operaciones $a=1$, $a+=1$, $a-=1$, $a*=2$, $a/=2$, $a\%=2$, a^=2, asignación
de valor a variable (var = value) y operador asignado. 

Ejemplo de *paso* y otras operaciones.

#+BEGIN_SRC awk
BEGIN{
paso=5;
contador=0;
print("Primer valor", contador);
contador+=paso;
print("Segundo valor", contador);
contador+=paso;
print("Tercer valor", contador);
contador+=paso;
print("Cuarto valor", contador);
contador-=paso;
print("Quinto valor", contador);
contador *=4;
print("Sexto valor", contador);
}
#+END_SRC

Las funciones matemáticas básicas sin, cos, tan, atan, sqrt, rand, srand,
atan2m, exp, log, están disponibles para su uso directo.


* Creación de funciones

** El concepto de función

Una función es la relación entre dos conjuntos. Un conjunto de partida o
*dominio* y un conjunto de salida o *recorrido*. Pueden ser matemáticas o
también de otros tipos.

#+CAPTION: Idea de función.
#+ATTR_LATEX: :width 0.5\textwidth
#+ATTR_HTML: :width 50% :height 50%                                                                                                                         
[[file:./img/funcion.png]]

** Entradas y salidas

Sin embargo, no toda función consiste en una operación matemática. Puede ser la
transformación de cualquier elemento o información en otro.

#+CAPTION: Función como máquina que opera entradas y salidas.
#+ATTR_HTML: :width 50% :height 50%                                                                                                                         
file:./img/in_out.png


** El uso de las funciones

La creación y uso de funciones nos serán útiles para ahorrar texto de código (no
repetirlo) y hacer que el mismo sea más legible para su lectura y estudio.

#+CAPTION: Uso de función en awk
#+ATTR_LATEX: :width 0.8\textwidth
#+ATTR_HTML: :width 80% :height 80%                                                                                                                         
[[file:./img/funcion_awk.png]]

Es ~awk~ las funciones se usan escribiendo su nombre y, entre paréntesis
poniendos sus argumentos o entradas.

** Relación entre triángulo y círculo

   Se tienen las dos figuras geométricas y se desea obtener un *círculo* que
   tenga la misma área que el *triángulo* dado.

   Se sabe que, dada la *base* y la *altura*, el área del tríangulo está dada
   por.

   \begin{equation}
Area_{triangulo}(base, altura) = \frac{base*altura} {2}
   \end{equation}

   Así como también el *área* de un círculo esa determinada por el *radio*.

   \begin{equation}
Area_{circulo}(radio) = \pi * radio^2
   \end{equation} 
   
 Por lo tanto, si ambas áreas son iguales.

\begin{equation}
Area_{triangulo}(radio) = Area_{triangulo}(base, altura)
\end{equation}

Despejando las ecuaciones.   

\begin{equation}
Relacion:= \pi*radio^2 = \frac{base*altura}{2}
\end{equation}

Se debe cumplir que, considerando que las dimensiones solamente son valores
positivos.

\begin{equation}
Radio_{círculo}(base, altura) = \sqrt {\frac{base*altura}{2*\pi}}
\end{equation}

De manera informática, esta *función* se puede escribir.

#+BEGIN_SRC awk
function radio_circulo(base, altura){
    PI=3.14159265;
    return sqrt(base*altura/(2*PI));
}
#+END_SRC

Así como también las funciones de área de las figuras.

#+BEGIN_SRC awk
function area_triangulo(base, altura){
    return base*altura/2;
}
function area_circulo(radio){
    PI=3.14159265;
    return PI*radio*radio;
}
#+END_SRC

De esta manera, desde un planteamiento o diseño del problema nos vamos acercando
a una solución computacional. El *script* que nos permitirá usar estas funciones
es.

#+BEGIN_SRC awk
  BEGIN{
      print("Relación de un círculo y un tríangulo de la misma área");
      infimo=.001;
  }
  {
  radio=radio_circulo($1,$2);
  print "La base es "$1,"La altura es "$2,"El radio es", radio;
  AT=area_triangulo($1,$2);
  AC=area_circulo(radio);
  print "Area tríangulo " AT, "Area circulo " AC;
  compara=AC>=AT-infimo && AC<=AT+infimo;
  mostrar=compara?"Iguales":"Distintos";
  print "Comparacion  de ambas figuras<",mostrar,">";
  }
#+END_SRC

El valor *ínfimo* se define para poder comparar y encontrar cercanía en
operaciones de *punto flotante* en que no es recomendable buscar igualdad
estricta. Se utiliza un valor muy pequeño /ínfimo/ que permita decir que lo que
resulta, el área del círculo, sea suficientemente semejante al área del triángulo.

El uso de este programa es llamando a *awk* con su script.
 
#+NAME: awk_f1
#+BEGIN_SRC shell  :session awk_f1 :exports code
awk -f ./ejercicios/triangulo_circulo.awk << EOF
2 3
4 5
2.3 7
8.9 2
EOF
#+END_SRC

Resulta que por cada línea en que se le ingresa un par de números (positvos),
realizará un cálculo para obtener el radio.

#+BEGIN_EXAMPLE
Relación de un círculo y un tríangulo de la misma área
La base es 2 La altura es 3 El radio es 0.977205
Area tríangulo 3 Area circulo 3
Comparacion  de ambas figuras< Iguales >
===
La base es 4 La altura es 5 El radio es 1.78412
Area tríangulo 10 Area circulo 10
Comparacion  de ambas figuras< Iguales >
===
La base es 6 La altura es 7 El radio es 2.58544
Area tríangulo 21 Area circulo 21
Comparacion  de ambas figuras< Iguales >
===
#+END_EXAMPLE

*** Ejercicio

    ¿Cómo sería la relación si del triángulo solamente conocemos los tres lados?

    Crear el programa partiendo de este supuesto.

    La ecuación que nos entrega el área del triángulo, dados los lados es:

\begin{equation}
Perimetro_{triangulo}(a,b,c)=a+b+c
\end{equation}

Dada *s* el semiperímetro: $s=Perimetro/2$.

\begin{equation}
Area_{triangulo}(a,b,c) = \sqrt{s*(s-a)*(s-b)*(s-c)}
\end{equation}

- ¿Cómo sería la relación del radio en función de a,b y c?
- ¿Qué condiciones tienen que darse para que a,b y c sean válidos?
- ¿Cómo se pueden programar esas condiciones?

** La temperatura en ciudades del mundo

El archivo *./ejercicios/ciudad_temp.csv* contiene la información de
*país,ciudad,temperatura*. Cada línea presenta un dato de temperatura para
una ciudad específica. 

Nuestra tarea es la siguiente:

- transformar temperatura de /°Celcius/ a /°Farenheit/.

La fórmula (que debe ser implementada como función) es:

$farenheit(celcius)=celcius*(\frac{9}{5})+32$

- alertar si la temperatura supera los 68°Farenheit poniendo el nombre de la
  ciudad en mayúsculas

** Crear una función de matemática

La creación de una función matemática en ~awk~ requiere que conozcamos las
reglas matemáticas con que operan, que valores se pueden operar, los paréntesis,
etc.

En este caso *implementar* una función significa que pasaremos la *expresión
algebraica* a un *código*. Conmúnmente llamada *fórmula*.

En un /nuevo archivo/, escribimos el programa, crea *alerta_ciudades.awk*, incluye
el siguiente código.

#+BEGIN_SRC awk
function celsius2farenheit(celsius)
{
    farenheit = celsius*(9/5)+32;
    return farenheit
}
#+END_SRC

** Crear una función que modifique un texto

Luego, teniendo el valor en *farenheit* debemos /evaluar/ y /comparar/. Si
supera un valor límite entonces ponemos el nombre de ciudad en *MAYÚSCULAS*

Agregamos el siguiente código.

#+BEGIN_SRC awk 
function alerta(ciudad, farenheit)
{
    city="";
    limite=68
    if (farenheit>=limite)
    {
        city = toupper(ciudad)}
    else{
        city = ciudad}
    return city
}
#+END_SRC

** Utilizando las funciones en el programa central

Una vez definidas las funciones, podemos utilizar estas en todos los programas
que consideremos necesarios. En este caso lo utilizaremos para leer el archivo y
entregar un reporte de *alerta*. Agregamos lo siguiente.

#+BEGIN_SRC awk 
BEGIN{
  vPAIS=1;
  vCIUDAD=2;
  vTEMP=3;
  FS=",";
  OFS=";";
  print "país","ciudad","temperatura °[F]"
}
(NR>1){
  FAR=celsius2farenheit($vTEMP);
  CIUDAD=alerta($vCIUDAD,FAR);
  print $vPAIS,CIUDAD,FAR
}
#+END_SRC

** Usar el programa y pasos siguientes

Utilizamos el *script awk* de la manera siguiente, utilizando la opción *-f*
para ejecutarlo y leer el archivo.

#+BEGIN_EXAMPLE
awk -f alerta_ciudades.awk ciudad_temp.csv
#+END_EXAMPLE

Si vemos que los resultados son correctos. 

- Guardar resultados a archivo: *comando>archivo*
- Comentar las líneas explicando que hace cada etapa
- Guardar el script en *https://gitlab.com/dashboard/snippets* para que quede de
  consulta.

* Creación de bibliotecas de funciones

** La filosofía de crear una biblioteca

Una *biblioteca de funciones* es un archivo que contiene una serie de
/funciones/ que pueden ser utilizadas en diferentes casos y programas.

[[file:./img/biblioteca_fn.png]]

** Agrupar funciones en un archivo

Continuaremos trabajando con el proyecto de *Temperaturas de Ciudades*.
Realizaremos lo siguiente.

- crear *funciones.awk*
- copiar las funciones *celsius2farenheit* y *alerta* al archivo
- borrar las funciones de *alerta_ciudades.awk*

Pasos a seguir: usando comandos bash

- Primero ver a qué líneas corresponde la sección de funciones *1 a 18*
- Luego enviar las líneas 1 a 18 a otro archivo *funciones.awk*
- Luego eliminar las líneas 1 a 18 del script.

** El código para separar biblioteca de script

Es así.

#+BEGIN_SRC bash
awk '{print NR, $0}' alerta_ciudades.awk
sed '1,18 w funciones.awk' alerta_ciudades.awk
sed -i '1,18 d' alerta_ciudades.awk
#+END_SRC

** Uso de biblioteca con opción "i"

Para usar las funciones de una biblioteca debemos decirle a ~awk~ que deseamos
usar una biblioteca de funciones, de esta manera se activan y quedan disponibles
para el uso.

#+BEGIN_EXAMPLE
awk -i funciones.awk -f alerta_ciudades.awk 
                        ciudad_temp.csv
#+END_EXAMPLE
 
O bien, de manera más explícita:

#+BEGIN_EXAMPLE
awk --include funciones.awk -f alerta_ciudades.awk 
                               ciudad_temp.csv
#+END_EXAMPLE

** Uso de biblioteca con @include

Dentro del mismo archivo *alerta_ciudades.awk*, al principio del código.

#+BEGIN_SRC awk
@include "funciones.awk"
#+END_SRC

Luego, se ejecuta de manera usual.

#+BEGIN_EXAMPLE
awk -f alerta_ciudades.awk ciudad_temp.csv
#+END_EXAMPLE

** Definir nombre a grupo de funciones, uso de *namespace*

Cuando tenemos la misión de definir y utilizar diversas funciones podemos
agruparlas bajo un nombre, que nos permita diferenciar.

Esta es una característica nueva, que aparece en ~awk v.5~ y permite potenciar
el uso de este lenguaje, puedes ver la [[https://www.gnu.org/software/gawk/manual/html_node/Namespaces.html][documentación]].

Por ejemplo, para el caso anterior, en que existen dos tipos de funciones. Ya
que ambas cumplen con objetivos diferentes pero entrelazados.

Podemos decir que:

- conversion :: para la función de conversión de temperaturas.

- criterio :: para la función alerta, ya que define un criterio-

Podemos entonces agregar un *namespace* de la siguiente manera, antes de cada
función escribir.

#+BEGIN_EXAMPLE
@namespace "conversion"
function celsius2farenheit(temp){
...
}

@namespace "criterio"
function alerta(temp, ciudad){
...
}
#+END_EXAMPLE
   
Se usaría de la siguiente manera.


#+BEGIN_SRC awk 
BEGIN{
  vPAIS=1;
  vCIUDAD=2;
  vTEMP=3;
  FS=",";
  OFS=";";
  print "país","ciudad","temperatura °[F]"
}
(NR>1){
  FAR=conversion::celsius2farenheit($vTEMP);
  CIUDAD=criterio::alerta($vCIUDAD,FAR);
  print $vPAIS,CIUDAD,FAR
}
#+END_SRC

Permite hacer más expresivo el código y entrega mejoes herramientas para
*debuggear* y hacer *profiling*.


* ¿Qué es debuggear?

  Cuando un programa tiene algún error o fallas no contempladas al momento de
  escribirlo y ejecutarlo se le llama *bug*, que son errores que deben ser
  corregidos.

  Para corregir errores será necesario ocupar herramientas como *print* para
  mostrar línea  a línea hasta donde llega correctamente. Así como también
  herramientas algo maś avanzadas.

  Cuando encuentras un punto interesante que te entrega algún problema, puedes
  determinar lo que se llama *breakpoint*.
  
Para iniciar el *debugger* puedes escoger la opción *-D* o *--debug*

#+BEGIN_EXAMPLE
awk -D -f alerta_ciudades.awk ciudad_temp.csv
#+END_EXAMPLE

O bien, de manera más explícita

#+BEGIN_EXAMPLE
awk --debug -f alerta_ciudades.awk ciudad_temp.csv
#+END_EXAMPLE

Si no tenemos llamada a la *biblioteca de funciones* entonces nos entregará el
siguiente error.

#+BEGIN_EXAMPLE
gawk> r
Starting program: 
país;ciudad;temperatura °[F]
awk: alerta_ciudades.awk:11: (FILENAME=ciudad_temp.csv FNR=2) fatal: function `celsius2farenheit' not defined
Program exited abnormally with exit value: 2
#+END_EXAMPLE

Nos dice que estamos usando una *función que no existe*. Lo corregimos,
incluimos la línea /@include "funciones.awk"/ y volvemos a revisar.

#+BEGIN_EXAMPLE
gawk> r
Starting program: 
país;ciudad;temperatura °[F]
Chile;SANTIAGO;77
Chile;Valdivia;55.4
chile;Arica;53.6
Argentina;BUENOS AIRES;91.4
Perú;Lima;59
perú;TACNA;71.6
Ecuador;QUITO;80.6
Estados Unidos;VIRGINIA;84.2
Estados Unidos;CALIFORNIA;98.6
México;Ciudad de México;59
Program exited normally with exit value: 0
gawk> q
#+END_EXAMPLE

El uso de *r* para ejecutar todo y luego *q* para salir. Hay dos acciones
importantes que nos serán de utilidad para debuggear y eso:

** Breakpoint

Hay tres formas de definir un breakpoint

- break function_name :: define una parada en la declaración de la función
  
- break numbero_linea(n) :: define parada en cierta línea

- break filename:n :: define el archivo y número de línea

Una vez que ya se definen las paradas se puede jugar con:


- r :: ejecutar hasta siguiente breakpoint

- s :: continua la ejecución hasta encontrar una llamada a una fuente, entrando
  en el análisis de la función.

- n :: ejecuta siguiente línea

- until n :: ejecuta n líneas más

- c :: continua ejecución del programa ignorando ya la parada.

Referencia de los comandos *gdb awk*, [[https://www.gnu.org/software/gawk/manual/html_node/Debugger-Execution-Control.html][aquí]].

Ejemplo.

#+BEGIN_EXAMPLE
gawk> b 2
Breakpoint 1 set at file `alerta_ciudades.awk', line 3
gawk> n
error: program not running.
gawk> r
Starting program: 
Stopping in BEGIN ...
Breakpoint 1, main() at `alerta_ciudades.awk':3
3         vPAIS=1;
gawk> n
4         vCIUDAD=2;
gawk> n
5         vTEMP=3;
gawk> n
6         FS=",";
gawk> n
7         OFS=";";
gawk> n
8         print "país", "ciudad", "temperatura °[F]"
gawk> n
país;ciudad;temperatura °[F]
Stopping in Rule ...
10      (NR>1){
gawk> n
11        FAR=celsius2farenheit($vTEMP);
gawk> n
12        CIUDAD=alerta($vCIUDAD,FAR);
gawk> s
alerta(ciudad, farenheit) at `funciones.awk':10
10          city="";
gawk> s
11          limite=68
gawk> s
12          if (farenheit>=limite)
gawk> 
14              city = toupper(ciudad)}
gawk> 
17          return city
gawk> 
main() at `alerta_ciudades.awk':12
12        CIUDAD=alerta($vCIUDAD,FAR);
gawk> 
13        print $vPAIS,CIUDAD,FAR
gawk> c
Chile;SANTIAGO;77
Chile;Valdivia;55.4
chile;Arica;53.6
Argentina;BUENOS AIRES;91.4
Perú;Lima;59
perú;TACNA;71.6
Ecuador;QUITO;80.6
Estados Unidos;VIRGINIA;84.2
Estados Unidos;CALIFORNIA;98.6
México;Ciudad de México;59
Program exited normally with exit value: 0
#+END_EXAMPLE

** Profiling

   Una vez que ya sabemos y hacemos que nuestro programa funcione correctamente
   podemos analizar la ejecución bloque a bloque del mismo.


   Para crear un perfil bastará con activar la opción, el programa se ejecutará
   y se guarda el análisis en un archivo.

 #+BEGIN_SRC bash
 awk --profile=profile.prof -f alerta_ciudades.awk ciudad_temp.csv
 #+END_SRC

 Si miramos, con las opciones básicas de configuración podremos ver cuantas
 veces se ejecuta cada trozo de código. Lo que nos da un índice de profundidad.
 Así como también es de ayuda para mejorar ostensiblemente el código, creando
 optimizaciones y mejores algoritmos.

   #+BEGIN_EXAMPLE
   	# perfil de gawk, creado Mon Nov 30 23:45:08 2020

	# BEGIN rule(s)

	BEGIN {
     1  	vPAIS = 1
     1  	vCIUDAD = 2
     1  	vTEMP = 3
     1  	FS = ","
     1  	OFS = ";"
     1  	print "pa\303\255s", "ciudad", "temperatura \302\260[F]"
	}

	# Regla(s)

    11  (NR > 1) { # 10
    10  	FAR = celsius2farenheit($vTEMP)
    10  	CIUDAD = alerta($vCIUDAD, FAR)
    10  	print $vPAIS, CIUDAD, FAR
	}


	# Funciones, enumeradas alfabéticamente

    10  function alerta(ciudad, farenheit)
	{
    10  	city = ""
    10  	limite = 68
    10  	if (farenheit >= limite) { # 6
     6  		city = toupper(ciudad)
     4  	} else {
     4  		city = ciudad
		}
    10  	return city
	}

    10  function celsius2farenheit(celsius)
	{
    10  	farenheit = celsius * (9 / 5) + 32
    10  	return farenheit
	}
#+END_EXAMPLE

Optimizar el código para que la variable *límite* se defina una sola vez.
