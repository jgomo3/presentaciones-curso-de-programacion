# previo diseño matemático de la relación
function radio_circulo(base, altura){
    PI=3.14159265;
    return sqrt(base*altura/(2*PI));
}
function area_triangulo(base, altura){
    return base*altura/2;
}
function area_circulo(radio){
    PI=3.14159265;
    return PI*radio*radio;
}
# EL PROGRAMA
BEGIN{
print("Relación de un círculo y un tríangulo de la misma área");
infimo=.001;
}
{
radio=radio_circulo($1,$2);
print "La base es "$1,"La altura es "$2,"El radio es", radio;
AT=area_triangulo($1,$2);
AC=area_circulo(radio);
print "Area tríangulo " AT, "Area circulo " AC;
compara=AC>=AT-infimo && AC<=AT+infimo;
# operador ternario:
mostrar=compara?"Iguales":"Distintos";
print "Comparacion  de ambas figuras<",mostrar,">";
print "===";
}
