#+LATEX_COMPILER: lualatex
#+TITLE: Textos en Bash, conversión  y edición con sed
#+AUTHOR: David Pineda Osorio
#+LANGUAGE: es
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+latex_header: \usepackage[title, titletoc]{appendix} 
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing

* Temario de la clase

** ¿Qué veremos en esta clase?

- Aprenderemos a transformar archivos pdf a texto :: con el fin de poder buscar
     y extraer la información que contienen.

- Aprenderemos a seleccionar y transformar cadenas de texto :: dentro de los
     archivos y streams, utilizando el comando ~sed~ y las acciones disponibles. 
     
* Registrar inicio de clases

** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *edición_sed*.

#+BEGIN_SRC bash
echo "CLASE_05::edicion_sed"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_05::edicion_sed*

* ¿Cómo seleccionamos y detectamos patrones en un texto?

Una serie de textos que podrían leídos de manera independientes pueden presentar
conexiones temáticas en diversos puntos. Las herramientas computacionales nos
servirán para tomarlos y realizar análisis sobre estos, dilucidando estas
conexiones.

Dependiendo de la complejidad de la herramienta será posible encontrar distintos
niveles de detalles. Desde una simple búsqueda por cadena de símbolos, como se
hace con *grep*, hasta implementaciones estadísticas de inteligencia artificial, entre otras.

* Convertir pdf a texto

Puede pasar que tenemos algunos archivos en formato *pdf*. Según lo que
ya hemos visto, este formato no es texto puro, contiene una serie de símbolos
binarios que son parte de su estandar. 

Una forma de analizar el texto que contiene un *pdf* será *extraer a texto* la
información relevante.

** Cuando el texto está en pdf

Vamos a instalar las utilidades de ~poppler~, que es un conjunto de herramientas
para la manipulación de ~pdf~ y conversión a diferentes formatos. 

Nuestro objetivo es /simplificar/ el acceso al texto obteniendo un archivo de texto
*ASCII*

#+BEGIN_SRC bash
sudo apt-get install poppler-utils
#+END_SRC

** Convierte pdf a txt

Vamos a convertir un archivo que contiene texto ~pdf~ a un archivo de 
texto, de manera que podamos acceder y manipular su contenido de manera
programática.

Tenemos el archivo *manifiesto_comunista.pdf* y lo convertiremos a un archivo
de texto.

#+BEGIN_SRC bash
pdftotext manifiesto_comunista.pdf manifiesto_comunista.txt
#+END_SRC

Abrimos el archivo y vemos que esté bien formateado, de lo contrario, hacemos lo que
sucede en la siguiente página.

* Ajustar texto a un tamaño de columna

Al momento de *copiar un texto* desde una página web nos encontraremos con el
problema de que el texto en el archivo /no está justificado/. Puede presentarse
cada párrafo como una línea continua.

La idea de esta sección es mostrar como se puede arreglar usando un comando y no
aprentando /enter/ cada tanto para crear los párafos.

** Obtener un texto desde la web

Vamos a buscar el *Discurso de Salvador Allende en la ONU*,

- url :: http://www.abacq.net/imagineria/cronolo4.htm
- copiar el texto (ctrl+shift+v)
- pegar un un archivo *salvador_allende_onu.txt*

Observamos que cada párrafo tiene una línea muy extensa a la derecha.

** Ajustar un texto para lectura en una pantalla

El texto descargado y copiado a un archivo necesita un poco más de trabajo, por
lo que será necesario ajustar la cantidad de columnas con texto disponible para 
dar una mayor legibilidad.

#+BEGIN_SRC bash
fold -w 80 -s salvador_allende_onu.txt > salvador_allende_onu_final.txt
#+END_SRC

De esa manera, obtenemos un texto presentable, ajustado a *80 caracteres*.

* Tarea para la casa: búsqueda y selección

Se ha creado una misión muy importante, es la *Tarea de búsqueda y selección*
para resolver /grupalmente/, es decir habla con tus compañeros/as de clase para
organizarse y realizar esta tarea en conjunto. 

Se ubica en el archivo *tarea_grep_sed.org* y describe los ejercicios a realizar
con los textos de:

- Greta Thunberg
- Salvador Allende
- Karl Marx
- Elinor Ostrom

Cada uno de ellos es un tanto distinto en sus formatos de presentación y
contenidos, pero están relacionados. La misión consiste en encontrar la relación
utilizando las herramientas aprendidas hasta ahora en el curso.

Nota: También puedes ver el archivo *pdf* o *html*. El archivo *org* es el original.

¡Éxito!

* Una herramienta que nos ayuda a seleccionar y modificar

Una vez que ya estamos acostumbrados a trabajar con *textos* es hora de conocer
uno de los comandos que nos permite editar, modificar o escribir textos.

Hay toda una genealogía de *sed*, ya que viene de un editor muy básico y
ancestral llamado *ed*, además de que provee algunas de las características de
búsqueda con grep.

El uso más común que se le da a *sed* es de /reemplazar programáticamente/. Pero
¡no es solo eso! Permite el trabajo con *expresiones regulares*, crear scripts y
¡hasta música!

Se puede decir que *sed* es un /lenguaje de programación/ especializado en texto.

** El comando ~sed~ para editar en la terminal

El comando ~sed~ es parte de la *trifuerza POSIX* y nos ayudará a seleccionar
textos desde /archivos o streams/, para eso debemos tener en cuenta que existen
en los textos:

- marcas en forma de cadenas de símbolos
- números de línea
- patrones de texto 
- frases especiales 

Con eso, podemos controlar que ver o no, que modificar o no. Para ubicarnos
siempre será de ayuda ~grep~ y los editores de texto.

Revisaremos el manual para revisar qué nos entrega este /comando/.

#+BEGIN_SRC bash
man sed
#+END_SRC

** Uso de sed tomando un archivo

Cada vez que necesites editar programáticamente un texto, puedes usar la
 estructura de ~sed~.

Para *leer un archivo* es la siguiente:

#+BEGIN_EXAMPLE
sed [OPCIONES] '{comandos sed}' ARCHIVO(s)
#+END_EXAMPLE

En dónde la parte *{comandos sed}* puede ser una o mas acciones
que se realizan sobre cada línea que se lea. 

- '1 comando sed'
- '{comando sed 1; comando sed 2}'

Podemos decir que *sed* es un /intérprete/ de algunos comandos especiales de edición.

** Uso de sed tomando un stream

Cuando ejecutamos un comando, el resultado de este retorna por la salida
estándar o de error. Lo que se muestra por la terminal es un *stream* y también
puede ser filtrado y editado.

De manera análoga al caso anterior, tomando desde un ~stream~ o ~salida estándar~.

#+BEGIN_EXAMPLE
cat ARCHIVO| sed [OPCIONES] '{comandos sed}'
#+END_EXAMPLE

** Cuando los comandos son varios, hacer script

Por cada /nueva línea/ que lee *sed* será posible procesar ese texto como
información y aplicarle no solo una sino varios comandos al mismo.

En caso que la cantidad de ~comandos sed~ sean muchas, es muy recomendable
hacer un ~script sed~.

Estos se pueden agrupar entre llaves *{}*, o bien enviarlos a un archivo en
limpio que contenga los comandos.

#+BEGIN_SRC bash
nano comandos.sed
#+END_SRC

#+BEGIN_EXAMPLE
comando sed 1;
comando sed 2;
comando sed 3;
#+END_EXAMPLE

Luego, se ejecuta el comando /general de sed/ así:

#+BEGIN_EXAMPLE
sed [OPCIONES] -f comandos.sed ARCHIVO(s)
#+END_EXAMPLE

Como se observa, se toma el archivo del *script*, con la opción *-f*.

* Aprender lo básico de sed

** Mostrar lo que se lee (imprimir en salida estándar)

Utilizando el archivo *ejercicios/texto1*, realizaremos las siguientes
pruebas de concepto para ver que tan /útil/ es la herramienta.

Imprimir la misma línea que entra como salida (comando  *p*), muestra lo que
entra y el resultado.

#+BEGIN_SRC bash
sed 'p' texto1
#+END_SRC

Si observas cuidadosamente, el uso de 'p' repite cada línea. Más adelante
veremos que, si se usa en combinación con otros comandos, muestra la línea
modificada.

En este caso, para mostrar una sola vez las líneas (opción *-n* operar en silencio), ya
que no se les realiza alguna modificación.

#+BEGIN_SRC bash
sed -n 'p' texto1
#+END_SRC

La opción *-n* oculta tanto la entrada o la salida. La única manera de ver que
está pasando cuando se usa esta opción es utilizar 'p' al lado de cada comando.

Cuando se usa solamente el comando *p* muestra la /misma/ línea que entra, más adelante se verá que un
 resultado de una línea modificada se puede también imprimir con /p/ o almacenar en un
 archivo.

Es /análogo/ a usar *cat* sobre el archivo:

#+BEGIN_SRC bash
cat texto1
#+END_SRC

A veces, cuando se programa un *script* es necesario no mostrar todos los
resultados de cada acción, por lo que se podría escoger activar la operación
/silenciosa/, con *-n*.

** Seleccionar líneas

EL comando *sed* puede identificar el número de línea del archivo que está
leyendo. Por lo tanto también puede filtrar o selecccionar.

Mostrar solo una línea *N=4*

#+BEGIN_SRC bash
sed -n '4 p' texto1
#+END_SRC

Mostrar la línea *N=4* a *M=8*

#+BEGIN_SRC bash
sed -n '4,8 p' texto1
#+END_SRC

Desde *N=4* hasta el final (uso de *$*)

#+BEGIN_SRC bash
sed -n '4,$ p' texto1
#+END_SRC

** Mostrar solo cada tantas líneas

Para mostrar solo aquellas líneas pares, impares o cada algún número
especial se usa el símbolo *~* llamado *cola de chancho* en Chile.

#+BEGIN_SRC bash
# impares
sed -n '1~2 p' texto1
# pares
sed -n '2~2 p' texto1
# cada 4 desde 3
sed -n '3~4 p' texto1
#+END_SRC

** Uso de patrones de texto

De manera análoga a seleccionar ciertas líneas específicas, podemos seleccionar
solamente aquellas líneas que contienen coincidencia con un patrón de texto.

#+BEGIN_SRC bash
sed -n '/Anna/ p' texto1
sed -n '/Pedro/ p' texto1
#+END_SRC

** Mostrar entre dos patrones de texto 

Desde que aparece /Anna/ hasta que aparece /Pedro/

#+BEGIN_SRC bash
sed -n '/Anna/,/Pedro/ p' texto1
#+END_SRC

¿Qué líneas hay entre medio?

** Mostrar las siguientes N líneas después de coincidir.

Si *N=4*, después de /Anna/ debería mostrar 4 líneas.

#+BEGIN_SRC bash
sed -n '/Anna/,+4 p' texto1
#+END_SRC

* Eliminar el texto bajo ciertas marcas, comando 'd'

** Borrar líneas con una coincidencia

Deseamos borrar todas las líneas que contengan /Pedro/, el uso de ~d~
al final nos permitirá filtrar o descartar la línea con esa coincidencia.

#+BEGIN_SRC bash
sed '/Pedro/ d' texto1
#+END_SRC

** Borrar líneas N a M

De manera análoga, para no mostrar desde línea *N a M*

#+BEGIN_SRC bash
sed 'N,M d' texto1
#+END_SRC

** Descartar o borrar líneas vacías.

Esta es muy útil, ya que suele utilizarse para limpiar archivos de líneas en blanco.

#+BEGIN_SRC bash
sed '/^$/ d' texto1
#+END_SRC

Utiliza una ~regex~ que dice que desde el inicio (^) a final ($) no hay nada.

* Escribir resultados a un archivo, comando 'w'

Ahora, resulta útil usar /w/ en combinación con la opción *-n*, todo lo que
resulte de operar con el comando se guardará en el archivo creado.

** Guardar las acciones de sed en un archivo

De manera interna ~sed~ dispone de la posibilidad de escribir los resultados de 
un comando a otro archivo, para eso se usa la estructura

#+BEGIN_SRC sed
comando_sed w archivo_salida.txt
#+END_SRC

De la manera siguiente, escribe todas las coincidencias de /Pedro/ a *otro_archivo*:

#+BEGIN_SRC bash
sed '/Pedro/ w otro_archivo' texto1
#+END_SRC

* Modificar el texto bajo ciertas marcas

Un uso muy común de sed es cambiar cosas. Cuando necesitamos cambiar todas
coincidencias de un texto, debemos considerar lo siguiente. 

- La lectura línea a línea de sed :: por cada línea que entra a ~sed~ se ejecuta
     el o los comandos.

- El conteo de coincidencias :: por cada coincidencias se identifica con una
     posición.

** Cambiar solo la primera coincidencia de cada línea 

Cambiar Pedro por Peter, solo la primera coincidencia *en la línea*

#+BEGIN_SRC bash
sed 's/Pedro/Peter/' texto1
#+END_SRC

Cambiar sueño por visión

#+BEGIN_SRC bash
sed 's/sueño/visión/' texto1
#+END_SRC

** Cambiar todo usando 'g'

Cambiar Pedro por Peter, *todas* las coincidencias.

#+BEGIN_SRC bash
sed 's/Pedro/Peter/g' texto1
#+END_SRC

Acá *g* hace referencia a ser *global o general*.

** Mostrar solo los cambios 

Cambiar Pedro por Peter, cuando cambie imprimir también el cambio.

#+BEGIN_SRC bash
sed -n 's/Pedro/Peter/g p' texto1
#+END_SRC

** Cambiar la coincidencia en la posición 'N'

Cambiar Pedro por Peter,  *N=2*, solo cambiar la segunda coincidencia.

#+BEGIN_SRC bash
sed 's/Pedro/Peter/2' texto1
#+END_SRC

** Ignorar mayúsculas de minúsculas

Compara, es sensible a tamaño de letra, debemos decir que no importa si es
mayúsculas o minúsculas (ignore-case).

#+BEGIN_SRC bash
sed -n 's/sueño/visión/gp' texto1
#+END_SRC

Añadiendo el comando  *i* al final del reemplazo, y *p* para mostrar el cambio.

#+BEGIN_SRC bash
sed -n 's/sueño/visión/gip' texto1
#+END_SRC

** Seleccionar entre N y M, cambiar y guardar 

Una combinación interesante, para las coincidencias de Pedro, entre 14 y 19,
guardar en archivo pedro_14_19.txt'

#+BEGIN_SRC bash
sed -n '14,19 s/Pedro/Peter/giw pedro_14_19.txt' texto1
#+END_SRC

* Intuición de un comando sed

De todas las opciones podemos deducir una forma general que permite construir un
comando interno de *sed* que sea /válido/.

** Usando la imaginación

En base a lo aprendido, la estructura general de un comando sed podría
comprenderse de la siguiente manera.

[[file:./img/intuicion.png]]

* Escribir varias acciones de tipo sed en un script
** Editar y crear nuevo archivo

Cualquier acción de ~sed~ sobre la que se cambie algo, puede registrarse
completa en la salida estándar (sin ocultar las líneas no modificadas).

Esto muestra todas las líneas del mismo archivo, y aquellas en que cambia las
mestra una vez cambiada. Luego la envía a otro archivo.

#+BEGIN_SRC bash
sed 's/Pedro/Peter/g' texto1 > cambios_pedro
#+END_SRC

Luego, revisa si existe el contenido.

#+BEGIN_SRC bash
cat cambios_pedro
#+END_SRC

** Editar mismo archivo, usando la opción '-i'

Cualquier acción de ~sed~ sobre la que se cambie algo, puede registrarse
completamente y cambiar en el mismo archivo. A veces es recomendable.

#+BEGIN_SRC bash
cp texto1 texto2
sed -i 's/Pedro/Peter/g' texto2
#+END_SRC

Luego, revisa si existe el contenido.

#+BEGIN_SRC bash
cat texto2
#+END_SRC

* El uso de expresiones regulares

Así como lo vimos con *grep*, la aplicación de expresiones regulares con *sed*
nos permitirá editar textos. Es decir, cambiar, reparar o corregir textos que
deban modificarse.
  
** Los patrones de texto SON expresiones regulares

Tomar el archivo *ejercicios/primer_regex.txt* y modificar 

- mamá -> superior 
- papá -> superior
- eliminar líneas vacías

Es decir, tomar la *expresion regular* que agrupa ambas 

#+BEGIN_EXAMPLE
[pm]a[pm]á
#+END_EXAMPLE

y reemplazarlas por *superior*, utilizando ~sed~. Además, incluir el comando 
que borre las líneas en blanco 

#+BEGIN_EXAMPLE
'/^$/ d'
#+END_EXAMPLE

* El conjunto de herramientas

Una visión general nos otorgará el discernimiento para escoger la herramienta
adecuada según lo que necesitemos. Revisemos la idea de *trifuerza POSIX*.

** La trifuerza POSIX

Dentro de todo el conjunto de herramientas disponibles del sistema
*POSIX*, lo podemos entender como la *trifuerza* esencial con
{~grep~, ~sed~, ~awk~} y el uso de las ~expresiones regulares~.

#+NAME:   fig: trifuerza_posix
#+ATTR_HTML: width="250px"
#+ATTR_ORG: :width 250
#+attr_latex: :width 250px
[[file:./img/trifuerza.png]]


Se ocupa el *brillante papel* de ser aquella herramienta para realizar ediciones
quirúrgicas sobre un texto sin romper su estructura general.

* Material de referencia

Los siguientes textos los podrás descargar, revisar y mantener como referencia
para tus labores de creación de comandos y /scripting/.

** Sobre sed

- Buscador Pirata Library Gen Ru :: http://gen.lib.rus.ec
- sed & awk :: Dale Dougherty, Arnold Robbins
- sed & awk :: Pocket Reference
- sed & awk :: Ramesh Natarajan
  
