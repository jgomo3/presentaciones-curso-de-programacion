# -*- org-confirm-babel-evaluate: nil -*-y
#+LATEX_COMPILER: lualatex
#+TITLE: Primeros comandos de Bash en GnuLinux, parte II
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+latex_class_options: [10pt]
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing

* Temario de la Clase

- La lógica del sistema POSIX ::

Aprenderemos las formas generales de como funciona un sistema de comandos como
~bash~ en GnuLinux. Esta es una definición estandarizada llamada ~POSIX~, que
dice como se construye la interfaz de comandos con el usuario.

 - Comandos para manipular directorios ::
Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar directorios.

* Registrar Inicio de Clases

** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+NAME: iniciar
#+BEGIN_SRC shell
echo "CLASE_02"
#+END_SRC

Deberías ver en la terminal:

#+RESULTS: iniciar
: CLASE_02

Anota en tu cuaderno la fecha y la seña *CLASE_02*

* Copiar los ejercicios de clase a tu carpeta de trabajo

En tu carpeta *Ejercicios_Programación*, o como le hayas puesto, crear la
carpeta *sesion_03* y, dentro de esta, copiar el contenido de *ejercicios*.

Contiene los archivos y directorios con que se trabajará en esta clase.


* Manipulación de directorios

** Crear 

Para crear carpetas (directorios) en un punto, tienes dos opciones:

- Conocer la ruta completa en el punto de creación
- Moverte hasta el directorio padre y crear la carpeta.
- Usar la opción *-p* para crear carpetas en profundidad.

#+BEGIN_SRC bash
mkdir NUEVA_CARPETA
#+END_SRC

Como ejercicio, en *raiz* crear las siguientes carpetas:

A nivel de /rama_a1 y rama_a2/:

- rama_a3
- rama_a3/rama_a31
- rama_a2/rama_a32
- rama_a4

En la /rama_b/:

- rama_b/rama_b2/rama_b22

** Mover o cambiar

Aprenderemos la noción de *origen* y *destino*.

- ¿Cúal es la carpeta origen? :: la que quieres mover
- ¿Cúal es la carpeta destino? :: el nuevo nombre de la carpeta

Tanto *origen* como *destino* son rutas a algún directorio. En el caso de mover,
consiste en cambiar lo que existe en *origen* a la posición *destino*.

#+BEGIN_SRC bash
mv ORIGEN DESTINO
#+END_SRC

Como ejercicio mover

- origen :: rama_a4
- destino :: rama_b/rama_b2/rama_b23

** Eliminar

Para eliminar directorios (y de manera recursiva, todo lo que hay adentro de
uno), será necesario usar la opción ~-r~.

#+BEGIN_SRC bash
rm -r DIRECTORIO
#+END_SRc

Como ejercicio, borrar la *rama_b2*

Opciones:

- ubica la ruta completa a *rama_b2*
- muévete hasta la /rama madre de/ *rama_b2*

#+BEGIN_SRC bash
rm -r ruta_b/rama_b2
#+END_SRc

* Buscar directorios o archivos

** ¿En qué consiste buscar?

Cuando realizamos la acción *buscar algo* lo que hacemos de manera inmediata:

- definir el espacio o *dominio* sobre lo que buscaremos
- definir el elemento o ese *algo* a buscar
- la búsqueda es exitosa si ese *algo* está, existe o pertenece al *dominio*

¿Cuándo buscas la llave de tu casa en tu habitación?

- dominio :: tu habitación
- elemento :: la llave
- condición de éxito :: encontrar la llave

Lo mismo ocurre en los sistemas informáticos.

Cuando buscamos un *archivo* debemos definir la *sección del disco duro* donde
podría encontrarse. Es decir, si el archivo pertenece a un directorio.

También, cuando buscamos una *palabra* en un *texto*. Lo que hacemos es *buscar*
una *secuencia ordenada de símbolos* que pertenenecen a una *secuencia ordenada de símbolos* mayor.

** Uso de /find/

Para buscar dentro de un directorio y todas sus ramas, el comando más útil será:

#+NAME: buscar
#+BEGIN_SRC shell :results output :exports both :session buscar :dir ./ejercicios
find -iname "archivo_semilla*"
#+END_SRC

Resulta las rutas a aquellos archivos que coinciden con lo que se busca.

#+RESULTS: buscar
: ./raiz/rama_b/rama_b1/archivo_semilla_nogal
: ./raiz/rama_b/rama_b1/archivo_semilla_baobab
: ./raiz/rama_a/rama_a2/rama_a22/archivo_semilla

La opción *iname* permite buscar por coincidencias sin importar si el caracter
está en /minúsculas/ o /mayúsculas/.

Buscar, desde raíz, los siguientes archivos o directorios

- archivo_semilla
- archivo_semilla*
- rama_a13

¿Qué resultados da? Anótalos en tus apuntes.

* Generalidades de la terminal bash
  
** Esquema de escritura de un comando

En general un comando de la terminal necesita una entrada, que puede tomar una por defecto si no se le da alguna, y 
los argumentos opcionales que permiten controlar cómo mostrar los resultados. 


#+CAPTION: Estructura general de un comando
#+NAME:   fig: comando
#+ATTR_HTML: width="200px"
#+ATTR_ORG: :width 200
#+attr_latex: :width 200px

[[file:./img/comando_gen.png]]

** Reflexión sobre la estructura de un comando

¿Cuándo usamos los siguientes comandos, a qué corresponde cada parte?

En la terminal, en tu carpeta de ejercicios personal de la ~clase_02~.

- cd listar
- ls -la
- ls -l *.png
- touch nuevo_archivo
- mv nuevo_archivo super_archivo
- mkdir nueva_carpeta
- mkdir -p nueva_carpeta/chiquitita/absurda

** Uso de la carpeta ejercicios/listar

Para comprender el uso de la terminal bash y sus características, usaremos,
dentro de la carpeta *./ejercicios/listar* los comandos que veamos a continuación.

** Esquema de salidas frente a un comando

Existen dos salidas frente a la ejecución de un comando

- stdout :: salida estándar o, lo que uno espera recibir

- stderr :: salida del error, si es que algo falla

#+CAPTION: Estructura general de un comando
#+NAME:   fig: comando
#+ATTR_HTML: width="300px"
#+ATTR_ORG: :width 300
#+attr_latex: :width 300px
[[file:./img/outputs.png]]
Lo que resulta de un comando, entonces, puede ser una salida *estándar* o bien
un *error*, pero ambas salidas son un *stream*.


** Encadenamiento de comandos con la pleca '|'

En *bash* se puede realizar una serie de acciones encadenadas o /'empipadas'/. El signo *|* permite
que una salida o respuesta a una acción o comando pueda ser tomada como entrada del siguiente comando.

Por ejemplo, si uno busca los archivos con extensión ~*.png~ cuyo nombre contenga una /a/.

#+BEGIN_SRC bash
ls -lsah *.png | grep a
#+END_SRC

** Salida a archivo, escribir nuevo o añadir nuevas líneas

Si en vez de querer ver la salida en la terminal, la deseas guardar en un archivo, es simple, utilizando
*>* o bien *>>* encadenadas al final del comando 

#+BEGIN_SRC bash
ls -lsah *.png | grep a > archivo_salida.txt
ls -lsah *.png | grep a >> archivo_salida.txt
#+END_SRC

*** Tarea para la casa

Investigar como se debe hacer para capturar en un archivo solamente 

- la salida estándar (los resultados correctos)
- la salida de error

Puede preguntar a l@s ayudantes.

* Crear un script básico

** Crear un script muy sencillo

Crear una carpeta *scripts*

Crear un archivo *hola_mundo.sh*

Ponerle el siguiente contenido al archivo: 

#+BEGIN_SRC  bash
echo "Hola mundo!"
#+END_SRC

** Correr un script usando el intérprete

Correr el script:

#+BEGIN_SRC  bash
bash hola_mundo.sh
#+END_SRC

** Crear un script que consulte el nombre

En su carpeta de trabajo personal, en la ~clase_02/scripts~.

Utilizando el comando ~read~ (ver el manual), escriba un script que pregunte el
nombre y le responda con:


#+BEGIN_SRC  bash
echo "¿Cuál es tu nombre?"
read NOMBRE
echo "Hola $NOMBRE, bienvenida/o 
al mundo de la programación!"
#+END_SRC

- ejemplos de uso :: https://www.computerhope.com/unix/bash/read.htm


* Resguardar los comandos realizados

** Guardar los comandos realizados en clases

Cada sesión realizaremos una serie de comandos, estos se almacenan en memoria.

En cada clase haz lo siguiente, dónde *NUMERO* es el número de la clase:

#+BEGIN_SRC bash
history
echo "CLASE NUMERO"
#+END_SRC

Al finalizar guarda los comandos realizados en un archivo. (es un comando que
veremos a futuro, pero úsalo). Crea antes la carpeta *historia*, en dónde te acomode.


- ¿Cuál era la seña que anotaste al principio?

#+BEGIN_SRC bash
history | grep -A 1000 "CLASE_02" > historia/clase_numero.sh
#+END_SRC

* Resumen de la clase

** ¿Qué aprendimos?

- Componer un comando con opciones y argumentos
- Encadenar comandos
- Buscar archivos o carpetas en un árbol de directorios
- Cambiar o mover elementos de directorios
- Crear un script sencillo
- Correr un script sencillo

** Nuevos comandos en esta clase

- mv :: mover o renombrar directorios o archivis
- mkdir -p :: crear directorios en profundidad 
- find :: hacer búsquedas de archivos o directorios extensivamente
- | :: uso de la *pleca* para encadenar comandos consecutivos
- rm :: para borrar archivos o directorios
- read :: para crear interacción con el usario
- grep :: búsquedas sencillas con grep
- ls +opciones :: uso de ls con opciones


* Ejercicios de práctica

** Un comando para comparar archivos

Puede suceder que necesites comparar contenidos entre archivos. Saber si son el
mismo o distintos, más allá si cambian el nombre.

Hay un comando llamado *fdupes* que analiza el una serie de directorios y
compara el contenido de cada archivo. 

Se instala así (en debian y sistemas de la familia deb):

#+BEGINS_SRC bash
sudo apt install fdupes
#+END_SRC

Se usa así:

#+BEGINS_SRC bash
fdupes directorio_1 directorio_2 directorio_3
#+END_SRC

También, si los archivos son distintos, se pueden compara línea a línea con el
comando *diff*.

#+BEGINS_SRC bash
diff -c archivo_1 archivo_2
#+END_SRC

La opción *c* permite mostrar cada archivo y marcar las líneas diferentes.

** Tarea para próxima clase

Vamos a crear una *selección literaria*, adicional a la selección del ejercicio de la clase anterior.

- tus 3 poemas preferidos
- tus 3 cuentos preferidos
- escoge un cuento de Horacio Quiroga, Julio Cortazar o Manuel Rojas.
- escoge 2 noticias que consideres importantes
- selecciona un ensayo de un autor/autora europeo/a
- selecciona un ensayo de un autor/autora latinoamericano/a
- selecciona 3 obras de artes visuales que te gusten
- haz un pequeño resumen de cada texto u obra

Responder las siguientes preguntas, anotar y mostrar como las resuelves:

- ¿Existe algún archivo en común?
- ¿Cómo puedes mostrar que existe en ambas selecciones (la otra es Cibernética y Comunidades)?
- ¿Cómo lo harías para mostrar solo el árbol de directorios?
- Si necesitas borrar alguna carpeta y todos sus archivos ¿Cómo lo harías?
- ¿Cómo lo harías para mostrar varios archivos que resulten de una búsqueda con *find*?



