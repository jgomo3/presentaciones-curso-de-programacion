archivo="ultimo.csv"
if [ -r "$archivo" ]; then
        size=$(stat --printf="%s" $archivo)
        modificacion=$(stat --printf="%y" $archivo)
        echo "El archivo "$archivo" existe y tiene un tamaño de "$size" bytes"
        echo "Además fue modificado por última vez en "$modificacion
elif [ -e "$archivo" ]; then
	echo "Existe pero no es de lectura"
    	sudo chmod +r $archivo
else
	echo "El archivo"$archivo" no existe"
fi
