  #!/usr/bin/awk -f
@include "palabras.awk"

BEGIN{
    OFS="|";
    print "title","word","amount of words"
}
{FNR==NR}{
    array_descarte[NR]=$0
}
(FNR==1){
    titulo=$0;
}
{
    gsub(/[[:punct:][:digit:]]/,"",$0);
    gsub(/[[:blank:]]{2}/," ",$0);
    for (pos=1;pos<=NF;pos++){
        word=tolower($pos);
        words[FILENAME][word]++
        total++
    }
}
ENDFILE{
    titles[FILENAME]=titulo
}
END{
    for (file in words){
        for (word in words[file]){
            if (descartar(word,array_descarte)==0){
                print titles[file], word, words[file][word]
            }
        }
    }
    total=0;
}
