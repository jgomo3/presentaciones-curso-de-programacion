#! /bin/bash
palabras=$(echo "$@" | tr [:upper:] [:lower:] | sed 's/[[:blank:]]/\n/g' | sort | uniq)
archivo=tabla_palabras.csv
for palabra in ${palabras[@]}; do
    grep --color "^$palabra," $archivo
done

