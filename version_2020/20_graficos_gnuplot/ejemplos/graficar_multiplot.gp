# se define layout o tamaño del multiploy
# filas x columnas
set terminal wxt size 1300, 600
set terminal pngcairo dashed enhanced
set output 'multiplot_temp.png'
set multiplot layout 2,1 rowsfirst
set grid
set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set style data histograms
set yrange [0:40]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set style fill solid 0.5
set boxwidth 0.5 relative
set key box
plot "temps_ciudades.csv" using 3 with histogram title "Temp. Santiago" lt rgb "#406090", \
     "" using 4:xtic(2) with histogram title "Temp. Pto Montt" lt rgb "#40FF00"
set yrange [-10:20]
set xrange [0:8]
set datafile separator ","
set style data lines
set key box
plot "temps_ciudades.csv" using 0:($3-$4):xtic(2) with line title "Diferencias de temperaturas"
unset multiplot
unset output
