set xlabel "Día"
set ylabel "Diferencia de Temperatura [°C]"
set datafile separator ","
set yrange [-10:20]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set xtics rotate by -45
set style fill solid 0.5
set term png
set output "temp_ciudades.png"
plot "< cat -" using 0:3:xtic(2) with lines title "Diferencias de Temperatura"
