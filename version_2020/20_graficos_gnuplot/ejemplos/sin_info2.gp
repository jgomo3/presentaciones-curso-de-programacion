set title "Sin(x)"
set xlabel "Tiempo [ms]" textcolor "blue"
set ylabel "Amplitud [m]" textcolor "red"
set key top left box opaque
set linestyle 1 \
  linecolor rgb "#FF7701" \
  linetype 1 linewidth 2 \
  pointtype 4 pointsize 1.5 
plot 4*sin(x) with linespoint linestyle 1
