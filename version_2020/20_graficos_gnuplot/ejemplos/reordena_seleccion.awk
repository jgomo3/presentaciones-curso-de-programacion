BEGIN{
    FS="|";
    OFS=",";
}
(NR>1){
    nombre=$1;
    palabra=$2;
    frecuencia=$3;
    array[palabra][nombre]=frecuencia;
    nombres[nombre]=1;
}
END{
    printf "palabra,";
    for (nombre in nombres){
        printf nombre","
    }
    printf "\n";
    for (palabra in array){
        printf palabra","
        for (nombre in nombres){
            f=array[palabra][nombre];
            frecuencia=f==0?"0":f;
            total+=f;
            printf "%d,", f;
        }
        printf total
        printf "\n"
        total=0;
    }
}
