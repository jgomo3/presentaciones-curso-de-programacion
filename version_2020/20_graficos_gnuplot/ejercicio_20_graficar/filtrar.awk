@include "pertenencia.awk"

BEGIN{
    FS=",";
    OFS=",";
    split(palabras, array_palabras, " ")
}
(NR==1){
    print
}
{
    palabra=$1;
    if (pertenencia(palabra,array_palabras)==1){
        print
    }
}
