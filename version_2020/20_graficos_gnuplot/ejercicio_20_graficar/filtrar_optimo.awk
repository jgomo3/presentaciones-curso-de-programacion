@include "pertenencia_opt.awk"

BEGIN{
    FS=",";
    OFS=",";
    split(palabras, array_palabras, " ");
    reverse(array_palabras, reverse_palabras)
}
(NR==1){
    print
}
{
    palabra=$1;
    if (pertenencia(palabra,reverse_palabras)==1){
        print
    }
}
